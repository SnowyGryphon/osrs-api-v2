# API V2 for OSRS Emu Bot
https://snowygryphon.com/products/osrs_emu_bot

Much more nice api according to all the feedback I've received the last 3 months.
This api is in **beta** and I will change and move around stuff until I am happy.
As of right now this API is for feedback and for trying it out.

# How to use this interface
In your script wrap the bot interface and do the following:

`new com.snowygryphon.osrs.script_v2.Bot(bot);`

# Features

## World Lookup
`bot.players().name("Chicken").closest().ifPresent(n -> n.interact("Attack");`

## Generic World Lookup
If you want to get air runes from both inventory and bank:

`static <T extends Item, STREAM extends GenericStream<T> & ItemStream> Optional<T> getItem(STREAM s) {
         s.name("Air rune");
         return s.first();
     }`

(Unfortunately due to deficiencies in the java language I have to have separate interfaces for generic and non generic code. But I've tried to make it as good as possible.)

And use it like this:

`var lp = bot.localPlayer().get();
         var inventory_item = getItem(lp.inventory().genericStream());
         var bank_item = getItem(lp.bank().genericStream());`

## Area API 

The lookup API is now integrated with the area api.
Allows to look for objects which is within an arbitrary area with ease.

``var area = new Area().addBox(new Tile(2500, 2500, 0), new Tile(2510, 2510, 0));``

``bot.players().within(area).random();``

Also has a bunch of other functionality to get the tile you are looking for.

closest/To

furthest/To

random

central

mostCentral

Constructors which allows code to be copied directly from https://explv.github.io/ area code generators.

## Interaction with doAction/interactWith
In the v1 API all the interaction is done by calling bot.interactWith.
While I like this in a data driven way it is often more convenient to have interact methods on world objects.
Therefore most code allows interact to be called directly on the object like in most OOP scripting.
See the world lookup example how much cleaner it is than the v1 equivalent.

## Interaction with tap and fellow method friends
Moved to a bot subobject to not clutter the base namespace too much.

``
bot.screen().tap(500, 300)
``

Is now the proper syntax.

## Randomization
uniform vs gausian distribution will always be a choice.

This current random implementation allows easy access to both of them.
It also has a next function, which will use uniform or gausian based on what has been configured beforehand.

The reimplementation of doUntil and sleepUntil makes use of this functionality while still allowing you guys to easily switch
implementation.

## doUntil and sleepUntil
They are both just as before in the base namespace, maybe I want to change this, or maybe this is good.

bot.doUntil and bot.sleepUntil works just as before.

But with the added benefit that you can now select a range of sleep and allow these methods to do the sleep for you. 

## Global access
After the first initialization of this V2 API.

``new com.snowygryphon.osrs.script_v2.Bot(bot);``

You can access the bot instance methods globally by Bot.tl().players()

Or even more conveniently B.players()

## Varbits

Many varbits has a boolean state,even though they are represented as
different bit patterns.

To make it easier to find document these values there is VarBitStateResolver
on which you can give it a varbit id and a custom function which figures out the boolean state.

``
HasCompletedTutorialIsland = new VarBitStateResolver(281, 1000);
``

After that you can use it as follows:
``
B.varbits().is(HasCompletedTutorialIsland)
``

For the varbits publicly known I try to document as many as possible in the VarBitState enum.
Which means you can do this:

``
B.varbits().is(VarBitState.HasCompletedTutorialIsland)
``

## Walking

Has been moved to a walking class.
Access it by:

``
B.walk().direct(pos)
``

## Future
This is the first major release, I will probably be doing a few more iterations and taking notes from you guys before I add it to the main distribution.