package com.snowygryphon.osrs.script_v2.geometry;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AreaTest {

    @Test
    void testBox() {
        var a = new Area().addBox(10, 20, 10, 20);
        run_box_tests(a);
    }

    @Test
    void testBoxTopLeft() {
        var a = new Area().addBoxTopLeft(10, 20, 20, 10);
        run_box_tests(a);
    }

    @Test
    void testBoxTopLeftConstructor() {
        var a = new Area(10, 20, 20, 10);
        run_box_tests(a);
    }

    @Test
    void testCircle() {
        var a = new Area().addCircle(new Position(15, 15, 0), 5);
        assertTrue(a.contains(new Position(10, 15, 0)));
        assertTrue(a.contains(new Position(15, 10, 0)));
        assertTrue(a.contains(new Position(15, 15, 0)));
        assertFalse(a.contains(new Position(10, 10, 0)));
        assertFalse(a.contains(new Position(15, 15, 1)));
    }

    @Test
    void testPoly() {
        var a = new Area(
                new int[][]{
                        { 3172, 3292 },
                        { 3172, 3298 },
                        { 3178, 3300 },
                        { 3183, 3296 },
                        { 3182, 3291 }
                }
        );

        assertTrue(a.contains(new Position(3173, 3294, 0)));
        assertFalse(a.contains(new Position(3173, 3294, 1)));
        assertTrue(a.contains(new Position(3172, 3292, 0)));
        assertFalse(a.contains(new Position(3171, 3292, 0)));
        assertFalse(a.contains(new Position(3169, 3293, 0)));
    }

    private void run_box_tests(Area a) {
        assertTrue(a.contains(new Position(10, 10, 0)));
        assertFalse(a.contains(new Position(9, 9, 0)));
        assertTrue(a.contains(new Position(20, 20, 0)));
        assertFalse(a.contains(new Position(21, 21, 0)));
        assertFalse(a.contains(new Position(10, 9, 0)));
        assertFalse(a.contains(new Position(9, 10, 0)));
        assertFalse(a.contains(new Position(21, 20, 0)));
        assertFalse(a.contains(new Position(20, 21, 0)));
        assertFalse(a.contains(new Position(10, 10, 1)));
    }
}