package com.snowygryphon.osrs.script_v2;

import com.snowygryphon.osrs.script.Camera;
import com.snowygryphon.osrs.script.Chat;
import com.snowygryphon.osrs.script.WorldSwitcher;
import com.snowygryphon.osrs.script.ui.RuntimeUI;
import com.snowygryphon.osrs.script_v2.log.Log;
import com.snowygryphon.osrs.script_v2.login.Login;
import com.snowygryphon.osrs.script_v2.path.Pather;
import com.snowygryphon.osrs.script_v2.screen.Screen;
import com.snowygryphon.osrs.script_v2.streams.GameObjectStream;
import com.snowygryphon.osrs.script_v2.streams.GroundObjectStream;
import com.snowygryphon.osrs.script_v2.streams.NPCStream;
import com.snowygryphon.osrs.script_v2.streams.PlayerStream;
import com.snowygryphon.osrs.script_v2.varbits.VarBits;
import com.snowygryphon.osrs.script_v2.walk.Walk;
import com.snowygryphon.osrs.script_v2.widget.Widget;
import com.snowygryphon.osrs.script_v2.world.*;
import com.snowygryphon.osrs.script_v2.ge.GrandExchange;
import com.snowygryphon.osrs.script_v2.stores.Prayers;
import com.snowygryphon.osrs.script_v2.random.Random;
import com.snowygryphon.osrs.script_v2.stores.Banking;
import com.snowygryphon.osrs.script_v2.stores.Equipment;
import com.snowygryphon.osrs.script_v2.stores.Inventory;
import com.snowygryphon.osrs.script_v2.stores.Spells;

import java.util.Optional;
import java.util.stream.Collectors;

public class Bot {
    private final com.snowygryphon.osrs.script.Bot bot;
    private final static ThreadLocal<Bot> threadlocal_bot = new ThreadLocal<>();

    public Bot(com.snowygryphon.osrs.script.Bot bot) {
        this.bot = bot;
        threadlocal_bot.set(this);
    }

    public static Bot threadlocal() {
        return threadlocal_bot.get();
    }

    public static Bot tl() { return threadlocal(); }

    public Optional<LocalPlayer> localPlayer() {
        var p = bot.getLocalPlayer();
        if(p == null) return Optional.empty();
        return Optional.of(new LocalPlayer(bot, p));
    }

    public PlayerStream players() {
        var players = bot.getPlayers().stream().map(p -> new Player(bot, p)).collect(Collectors.toList());
        return new PlayerStream(players, bot);
    }

    public NPCStream npcs() {
        var npcs = bot.getNPCs().stream().map(p -> new NPC(bot, p)).collect(Collectors.toList());
        return new NPCStream(npcs, bot);
    }

    public GameObjectStream gameObjects() {
        var o = bot.getGameObjects().stream().map(p -> new GameObject(bot, p)).collect(Collectors.toList());
        return new GameObjectStream(o, bot);
    }

    public GroundObjectStream groundObjects() {
        var o = bot.getGroundObjects().stream().map(p -> new GroundObject(bot, p)).collect(Collectors.toList());
        return new GroundObjectStream(o, bot);
    }

    public Equipment equipment() {
        return new Equipment(bot, inventory());
    }

    public Banking bank() {
        return new Banking(bot, inventory());
    }

    public Inventory inventory() {
        return new Inventory(bot);
    }

    public Walk walk() {
        return new Walk(bot);
    }

    public VarBits varBits() {
        return new VarBits(bot);
    }

    public Random random() {
        return new Random(bot);
    }

    public Spells spells() {
        return new Spells(bot);
    }

    public Prayers prayers() { return new Prayers(bot); }

    public RuntimeUI runtimeUI() {
        return bot.getRuntimeUI();
    }

    public Log log() {
        return new Log(bot);
    }

    public Login login() {
        return new Login(bot);
    }

    public Chat chat() {
        return bot.getChat();
    }

    public WorldSwitcher worlds() {
        return bot.getWorldSwitcher();
    }

    public Pather path() {
        return new Pather(bot);
    }

    public Camera camera() {
        return bot.getCamera();
    }

    public Widget widgetRoot() {
        var w = bot.getRootWidget();
        if(w == null) return null;
        return new Widget(bot, w);
    }

    public Screen screen() {
        return new Screen(bot);
    }

    public GrandExchange ge() {
        return new GrandExchange(bot, this);
    }

    public boolean doUntil(int max_len_ms, com.snowygryphon.osrs.script.Bot.IsCompletedCallback cb, Runnable r) {
        if(cb == null) throw new IllegalArgumentException("cb is null");
        if(r == null) throw new IllegalArgumentException("r is null");
        return bot.doUntil(max_len_ms, cb, r);
    }

    public boolean doUntil(int min, int max, com.snowygryphon.osrs.script.Bot.IsCompletedCallback cb, Runnable r) {
        if(cb == null) throw new IllegalArgumentException("cb is null");
        if(r == null) throw new IllegalArgumentException("r is null");
        return doUntil(random().next(min, max), cb, r);
    }

    public boolean sleepUntil(int max_len_ms, com.snowygryphon.osrs.script.Bot.IsCompletedCallback cb) {
        return bot.sleepUntil(max_len_ms, cb);
    }

    public boolean sleepUntil(int min, int max, com.snowygryphon.osrs.script.Bot.IsCompletedCallback cb) {
        if(cb == null) throw new IllegalArgumentException("cb is null");
        return sleepUntil(random().next(min, max), cb);
    }
}
