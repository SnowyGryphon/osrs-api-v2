package com.snowygryphon.osrs.script_v2;

import com.snowygryphon.osrs.script.Camera;
import com.snowygryphon.osrs.script.Chat;
import com.snowygryphon.osrs.script.WorldSwitcher;
import com.snowygryphon.osrs.script.ui.RuntimeUI;
import com.snowygryphon.osrs.script_v2.log.Log;
import com.snowygryphon.osrs.script_v2.login.Login;
import com.snowygryphon.osrs.script_v2.path.Pather;
import com.snowygryphon.osrs.script_v2.screen.Screen;
import com.snowygryphon.osrs.script_v2.stores.*;
import com.snowygryphon.osrs.script_v2.varbits.VarBits;
import com.snowygryphon.osrs.script_v2.walk.Walk;
import com.snowygryphon.osrs.script_v2.widget.Widget;
import com.snowygryphon.osrs.script_v2.ge.GrandExchange;
import com.snowygryphon.osrs.script_v2.random.Random;
import com.snowygryphon.osrs.script_v2.streams.GameObjectStream;
import com.snowygryphon.osrs.script_v2.streams.GroundObjectStream;
import com.snowygryphon.osrs.script_v2.streams.NPCStream;
import com.snowygryphon.osrs.script_v2.streams.PlayerStream;

public class B {
    public static Bot instance() {
        return Bot.tl();
    }

    public static PlayerStream players() {
        return Bot.tl().players();
    }

    public static NPCStream npcs() {
        return Bot.tl().npcs();
    }

    public static GameObjectStream gameObjects() {
        return Bot.tl().gameObjects();
    }

    public static GroundObjectStream groundObjects() {
        return Bot.tl().groundObjects();
    }

    public static Equipment equipment() {
        return Bot.tl().equipment();
    }

    public static Banking bank() {
        return Bot.tl().bank();
    }

    public static Inventory inventory() {
        return Bot.tl().inventory();
    }

    public static Walk walk() {
        return Bot.tl().walk();
    }

    public static VarBits varBits() {
        return Bot.tl().varBits();
    }

    public static Random random() {
        return Bot.tl().random();
    }

    public static Spells spells() {
        return Bot.tl().spells();
    }

    public static Prayers prayers() { return Bot.tl().prayers(); }

    public static RuntimeUI runtimeUI() {
        return Bot.tl().runtimeUI();
    }

    public static Log log() {
        return Bot.tl().log();
    }

    public static Login login() {
        return Bot.tl().login();
    }

    public static Chat chat() {
        return Bot.tl().chat();
    }

    public static WorldSwitcher worlds() {
        return Bot.tl().worlds();
    }

    public static Pather path() {
        return Bot.tl().path();
    }

    public static Camera camera() {
        return Bot.tl().camera();
    }

    public static Widget widgetRoot() {
        return Bot.tl().widgetRoot();
    }

    public static Screen screen() {
        return Bot.tl().screen();
    }

    public static GrandExchange ge() {
        return Bot.tl().ge();
    }

    public boolean doUntil(int max_len_ms, com.snowygryphon.osrs.script.Bot.IsCompletedCallback cb, Runnable r) {
        return Bot.tl().doUntil(max_len_ms, cb, r);
    }

    public boolean doUntil(int min, int max, com.snowygryphon.osrs.script.Bot.IsCompletedCallback cb, Runnable r) {
        return Bot.tl().doUntil(min,max, cb, r);
    }

    public boolean sleepUntil(int max_len_ms, com.snowygryphon.osrs.script.Bot.IsCompletedCallback cb) {
        return Bot.tl().sleepUntil(max_len_ms, cb);
    }

    public boolean sleepUntil(int min, int max, com.snowygryphon.osrs.script.Bot.IsCompletedCallback cb) {
        return Bot.tl().sleepUntil(min, max, cb);
    }
}
