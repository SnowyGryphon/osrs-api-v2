package com.snowygryphon.osrs.script_v2.props;

public interface SlotIDable {
    int slotID();
}
