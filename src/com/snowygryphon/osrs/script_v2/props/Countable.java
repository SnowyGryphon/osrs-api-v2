package com.snowygryphon.osrs.script_v2.props;

public interface Countable {
    int count();
}
