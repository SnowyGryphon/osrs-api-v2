package com.snowygryphon.osrs.script_v2.props;

import com.snowygryphon.osrs.script_v2.geometry.Position;
import com.snowygryphon.osrs.script_v2.Bot;
import com.snowygryphon.osrs.script_v2.geometry.Area;

public abstract class Positionable {

    public abstract Position tile();

    public boolean samePlane(Positionable other) {
        var t = tile();
        if(other == null) throw new IllegalArgumentException("other is null");
        return t.z == other.tile().z;
    }

    public double distanceTo(Position b) {
        if(!samePlane(b)) return Double.NaN;
        var t = tile();
        float diff_x = t.x - b.x;
        float diff_y = t.y - b.y;
        float diff_z = t.z - b.z;
        return Math.sqrt( diff_x * diff_x + diff_y * diff_y + diff_z * diff_z );
    }

    public double distanceTo(Positionable p) {
        if (p == null) throw new IllegalArgumentException("p is null");
        return distanceTo(p.tile());
    }

    public double distance() {
        return Bot.threadlocal().localPlayer().map(this::distanceTo).orElse(0D); // TODO: Do I want to return nan in this case?
    }

    public boolean within(Area a) {
        return a.contains(this);
    }

    public boolean withinCircle(Positionable center, double distance) {
        if(center == null) throw new IllegalArgumentException("center is null");
        return new Area().addCircle(center, distance).contains(this);
    }

    public boolean withinBox(Position bottom_left, Position top_right) {
        if(bottom_left == null) throw new IllegalArgumentException("bottom_left is null");
        if(top_right == null) throw new IllegalArgumentException("top_right is null");
        return new Area().addBox(bottom_left, top_right).contains(this);
    }
}
