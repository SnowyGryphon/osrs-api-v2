package com.snowygryphon.osrs.script_v2.props;

public interface Interactable {
    void interact(String action);
}
