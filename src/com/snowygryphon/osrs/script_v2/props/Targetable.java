package com.snowygryphon.osrs.script_v2.props;

import com.snowygryphon.osrs.script_v2.world.Character;

import java.util.Optional;

/**
 * Represents an object which can target other things. It is not an object which itself is targetable.
 */
public interface Targetable {
    Optional<Character> target();
}
