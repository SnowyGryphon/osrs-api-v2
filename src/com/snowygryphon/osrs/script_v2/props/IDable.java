package com.snowygryphon.osrs.script_v2.props;

public interface IDable {
    int id();
}
