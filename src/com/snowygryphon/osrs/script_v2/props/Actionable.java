package com.snowygryphon.osrs.script_v2.props;

import java.util.List;

public interface Actionable {
    List<String> actions();
    boolean hasAction(String ... action);
}
