package com.snowygryphon.osrs.script_v2.varbits;

public enum VarBitState {
    HasCompletedTutorialIsland(281, 1000),
    IsRunActivated(173, 1)
    ;

    VarBitState(int id, VarBitStateResolver.IsSet isSetCB) {
        handler = new VarBitStateResolver(id, isSetCB);
    }

    VarBitState(int id, int set_v) {
        this(id, v -> v == set_v);
    }

    private final VarBitStateResolver handler;

    public boolean isSet(int[] bits) {
        return handler.isSet(bits);
    }

    public VarBitStateResolver handler() {
        return handler;
    }
}
