package com.snowygryphon.osrs.script_v2.varbits;

import com.snowygryphon.osrs.script.Bot;

public class VarBits {
    private final Bot bot;

    public VarBits(Bot bot) {
        this.bot = bot;
    }

    int[] all() {
        return bot.getVarBits();
    }

    int get(int id) {
        var bits = bot.getVarBits();
        if(bits == null) return 0;
        if(id < 0 || id >= bits.length) return 0;
        return bits[id];
    }

    boolean is(VarBitStateResolver h) {
        if(h == null) throw new IllegalArgumentException("h is null");
        return h.isSet(all());
    }

    boolean is(VarBitState s) {
        if(s == null) throw new IllegalArgumentException("s is null");
        return is(s.handler());
    }
}
