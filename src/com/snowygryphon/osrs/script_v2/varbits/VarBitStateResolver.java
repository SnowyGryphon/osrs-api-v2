package com.snowygryphon.osrs.script_v2.varbits;

public class VarBitStateResolver {
    public VarBitStateResolver(int id, IsSet isSetCB) {
        this.id = id;
        this.isSetCB = isSetCB;
    }

    public VarBitStateResolver(int id, int set_v) {
        this(id, v -> v == set_v);
    }

    public interface IsSet {
        boolean isSet(int v);
    }

    private final int id;
    private final IsSet isSetCB;

    public boolean isSet(int[] bits) {
        if(id < 0 || id >= bits.length) return false;
        return isSetCB.isSet(bits[id]);
    }

    public int id() {
        return id;
    }
}
