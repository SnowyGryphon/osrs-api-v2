package com.snowygryphon.osrs.script_v2.background_code;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.BotConfig;
import com.snowygryphon.osrs.script.EnergyManager;
import com.snowygryphon.osrs.script.GenericSettingsHandler;

public class BackgroundCode {
    private final Bot bot;

    public BackgroundCode(Bot bot) {
        this.bot = bot;
    }

    public EnergyManager energyManager() {
        return bot.getEnergyManager();
    }

    public GenericSettingsHandler genericSettingsHandler() {
        return bot.getGenericSettingsHandler();
    }

    public BotConfig botConfig() {
        return bot.getBotConfig();
    }
}
