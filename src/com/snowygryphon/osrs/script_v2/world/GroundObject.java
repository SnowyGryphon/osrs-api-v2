package com.snowygryphon.osrs.script_v2.world;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.props.IDable;
import com.snowygryphon.osrs.script_v2.world.item.Item;
import com.snowygryphon.osrs.script_v2.world.item.ItemType;

public class GroundObject extends WorldObject implements IDable, Item {
    private final com.snowygryphon.osrs.script.GroundObject obj;

    public GroundObject(Bot bot, com.snowygryphon.osrs.script.GroundObject w) {
        super(bot, w);
        this.obj = w;
    }

    @Override
    public int id() {
        return obj.getItemID();
    }

    @Override
    public ItemType type() {
        return ItemType.Ground;
    }
}
