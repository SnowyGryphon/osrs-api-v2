package com.snowygryphon.osrs.script_v2.world;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.EquipmentSlot;
import com.snowygryphon.osrs.script_v2.world.item.WornItem;

import java.util.Map;
import java.util.stream.Collectors;

public class Player extends Character {
    private final com.snowygryphon.osrs.script.Player p;

    public Player(Bot bot, com.snowygryphon.osrs.script.Player ch) {
        super(bot, ch);
        this.p = ch;
    }

    public boolean localPlayer() {
        return p.isLocalPlayer();
    }

    public void onSkulled(com.snowygryphon.osrs.script.Player.OnSkullStatusChanged listener) {
        p.setOnSkullStatusChangedListener(listener);
    }

    public int overHeadIcon() {
        return p.getOverHeadIcon();
    }

    public int skullIcon() {
        return p.getSkullIcon();
    }

    public boolean hasOverHeadIcon() {
        return overHeadIcon() != -1;
    }

    public boolean skulled() {
        return skullIcon() != -1;
    }

    public int combatLevel() {
        return p.getCombatLevel();
    }

    public Map<EquipmentSlot, WornItem> equipment() {
        return p.getEquipment()
                .values()
                .stream()
                .map(WornItem::new)
                .collect(Collectors.toUnmodifiableMap(WornItem::slot, w -> w));
    }
}
