package com.snowygryphon.osrs.script_v2.world.item;

import java.util.List;

public abstract class VirtualItem implements Item {
    private final com.snowygryphon.osrs.script.Item i;

    public VirtualItem(com.snowygryphon.osrs.script.Item i) {
        this.i = i;
    }

    @Override
    public List<String> actions() {
        return i.getActions();
    }

    @Override
    public boolean hasAction(String... actions) {
        var actual = this.actions();
        for(String a : actions) {
            if(a == null) throw new IllegalArgumentException("a is null");
            if(actual.contains(a)) return true;
        }
        return false;
    }

    @Override
    public String name() {
        return i.getName();
    }
}
