package com.snowygryphon.osrs.script_v2.world.item;

import com.snowygryphon.osrs.script_v2.props.Actionable;
import com.snowygryphon.osrs.script_v2.props.Interactable;
import com.snowygryphon.osrs.script_v2.props.Nameable;

public interface Item extends Nameable, Actionable, Interactable {
    ItemType type();
}
