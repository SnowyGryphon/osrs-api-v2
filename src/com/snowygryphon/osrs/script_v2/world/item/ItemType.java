package com.snowygryphon.osrs.script_v2.world.item;

public enum ItemType {
    Bank,
    Inventory,
    Equipment,
    Ground
}
