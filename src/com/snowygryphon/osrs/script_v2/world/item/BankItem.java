package com.snowygryphon.osrs.script_v2.world.item;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.props.Countable;

public class BankItem extends VirtualItem implements Countable, Item {
    private Bot bot;
    private com.snowygryphon.osrs.script.BankItem i;

    public BankItem(Bot bot, com.snowygryphon.osrs.script.BankItem i) {
        super(i);
        this.bot = bot;
        this.i = i;
    }

    @Override
    public int count() {
        return i.getCount();
    }

    @Override
    public ItemType type() {
        return ItemType.Bank;
    }

    @Override
    public void interact(String action) {
        bot.interactWith(i, action);
    }
}
