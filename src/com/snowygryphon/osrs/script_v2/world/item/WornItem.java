package com.snowygryphon.osrs.script_v2.world.item;

import com.snowygryphon.osrs.script.EquipmentItem;
import com.snowygryphon.osrs.script.EquipmentSlot;
import com.snowygryphon.osrs.script_v2.props.IDable;
import com.snowygryphon.osrs.script_v2.props.Nameable;
import com.snowygryphon.osrs.script_v2.props.SlotIDable;

public class WornItem implements Nameable, IDable, SlotIDable {
    private final com.snowygryphon.osrs.script.EquipmentItem item;

    public WornItem(EquipmentItem item) {
        this.item = item;
    }


    @Override
    public String name() {
        return item.getName();
    }

    public EquipmentSlot slot() {
        return item.getSlot();
    }

    @Override
    public int slotID() {
        return item.getSlotID();
    }

    @Override
    public int id() {
        return item.getItemID();
    }
}
