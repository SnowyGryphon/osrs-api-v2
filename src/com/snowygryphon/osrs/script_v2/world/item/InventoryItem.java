package com.snowygryphon.osrs.script_v2.world.item;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.ItemAction;
import com.snowygryphon.osrs.script.WorldObject;
import com.snowygryphon.osrs.script_v2.props.Countable;
import com.snowygryphon.osrs.script_v2.props.IDable;
import com.snowygryphon.osrs.script_v2.props.SlotIDable;

public class InventoryItem extends VirtualItem implements IDable, Countable, SlotIDable, Item {
    private final Bot bot;
    private final com.snowygryphon.osrs.script.InventoryItem i;

    public InventoryItem(Bot bot, com.snowygryphon.osrs.script.InventoryItem i) {
        super(i);
        this.bot = bot;
        this.i = i;
    }

    public com.snowygryphon.osrs.script.InventoryItem getBackend() {
        return i;
    }

    @Override
    public int id() {
        return i.getItemID();
    }

    @Override
    public ItemType type() {
        return ItemType.Inventory;
    }

    @Override
    public void interact(String action) {
        bot.interactWith(i, action);
    }

    public void interact(int id, String str1, String str2) {
        if(str1 == null) throw new IllegalArgumentException("str1 is null");
        if(str2 == null) throw new IllegalArgumentException("str2 is null");
        bot.interactWith(i, id, str1, str2);
    }

    public void interact(int id, String str1) {
        if(str1 == null) throw new IllegalArgumentException("str1 is null");
        bot.interactWith(i, id, str1);
    }

    public void interact(int id) {
        bot.interactWith(i, id);
    }

    public void interact(ItemAction a) {
        if(a == null) throw new IllegalArgumentException("a is null");
        bot.interactWith(i, a);
    }

    public void useOn(InventoryItem other) {
        if(other == null) throw new IllegalArgumentException("other is null");
        bot.getInventory().useItemOn(i, other.i);
    }

    public void useOn(WorldObject obj) {
        if(obj == null) throw new IllegalArgumentException("obj is null");
        bot.getInventory().useItemOn(i, obj);
    }

    @Override
    public int count() {
        return i.getCount();
    }

    @Override
    public int slotID() {
        return i.getSlotID();
    }
}
