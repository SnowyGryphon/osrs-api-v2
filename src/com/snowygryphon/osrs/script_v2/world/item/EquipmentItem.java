package com.snowygryphon.osrs.script_v2.world.item;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.EquipmentSlot;
import com.snowygryphon.osrs.script_v2.props.Countable;
import com.snowygryphon.osrs.script_v2.props.IDable;
import com.snowygryphon.osrs.script_v2.props.SlotIDable;

public class EquipmentItem extends VirtualItem implements IDable, Countable, SlotIDable, Item {
    private final Bot bot;
    private final com.snowygryphon.osrs.script.EquipmentItem i;

    public EquipmentItem(Bot bot, com.snowygryphon.osrs.script.EquipmentItem i) {
        super(i);
        this.i = i;
        this.bot = bot;
    }

    @Override
    public int id() {
        return i.getItemID();
    }

    @Override
    public ItemType type() {
        return ItemType.Equipment;
    }

    @Override
    public void interact(String action) {
        if(action == null) throw new IllegalArgumentException("action is null");
        bot.interactWith(i, action);
    }

    @Override
    public int count() {
        return i.getCount();
    }

    @Override
    public int slotID() {
        return i.getSlotID();
    }

    public EquipmentSlot slot() {
        return i.getSlot();
    }
}
