package com.snowygryphon.osrs.script_v2.world;

import com.snowygryphon.osrs.script.Skill;

public class Skills {
    private final com.snowygryphon.osrs.script.Skills skills;

    public Skills(com.snowygryphon.osrs.script.Skills skills) {
        this.skills = skills;
    }

    public SkillState get(Skill s) {
        if(s == null) throw new IllegalArgumentException("s is null");
        return new SkillState(skills.get(s));
    }

    public SkillState attack() {
        return get(Skill.Attack);
    }

    public SkillState strength() {
        return get(Skill.Strength);
    }

    public SkillState ranged() {
        return get(Skill.Ranged);
    }

    public SkillState prayer() {
        return get(Skill.Prayer);
    }

    public SkillState magic() {
        return get(Skill.Magic);
    }

    public SkillState runecrafting() {
        return get(Skill.Runecraft);
    }

    public SkillState construction() {
        return get(Skill.Construction);
    }

    public SkillState hp() {
        return get(Skill.Hitpoints);
    }

    public SkillState agility() {
        return get(Skill.Agility);
    }

    public SkillState herblore() {
        return get(Skill.Herblore);
    }

    public SkillState thieving() {
        return get(Skill.Thieving);
    }

    public SkillState crafting() {
        return get(Skill.Crafting);
    }

    public SkillState fletching() {
        return get(Skill.Fletching);
    }

    public SkillState slayer() {
        return get(Skill.Slayer);
    }

    public SkillState hunting() {
        return get(Skill.Hunter);
    }

    public SkillState mining() {
        return get(Skill.Mining);
    }

    public SkillState smithing() {
        return get(Skill.Smithing);
    }

    public SkillState fishing() {
        return get(Skill.Fishing);
    }

    public SkillState cooking() {
        return get(Skill.Cooking);
    }

    public SkillState firemaking() {
        return get(Skill.Firemaking);
    }

    public SkillState woodcutting() {
        return get(Skill.Woodcutting);
    }

    public SkillState farming() {
        return get(Skill.Farming);
    }
}
