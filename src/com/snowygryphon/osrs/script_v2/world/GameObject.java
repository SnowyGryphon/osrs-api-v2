package com.snowygryphon.osrs.script_v2.world;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.props.IDable;

public class GameObject extends WorldObject implements IDable {
    private final com.snowygryphon.osrs.script.GameObject obj;

    public GameObject(Bot bot, com.snowygryphon.osrs.script.GameObject w) {
        super(bot, w);
        this.obj = w;
    }

    @Override
    public int id() {
        return obj.getObjectID();
    }
}
