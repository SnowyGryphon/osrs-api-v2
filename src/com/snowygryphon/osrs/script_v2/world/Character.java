package com.snowygryphon.osrs.script_v2.world;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.props.Actionable;
import com.snowygryphon.osrs.script_v2.props.Animatable;
import com.snowygryphon.osrs.script_v2.props.Movable;
import com.snowygryphon.osrs.script_v2.props.Targetable;

import java.util.Optional;

public class Character extends WorldObject implements Animatable, Movable, Actionable, Targetable {
    private final Bot bot;
    private final com.snowygryphon.osrs.script.Character ch;

    public Character(Bot bot, com.snowygryphon.osrs.script.Character ch) {
        super(bot, ch);
        this.bot = bot;
        this.ch = ch;
    }

    @Override
    public int anim() {
        return ch.getAnimation();
    }

    @Override
    public boolean moving() {
        return ch.isMoving();
    }

    public boolean isAnimating() {
        return anim() != -1;
    }

    @Override
    public Optional<Character> target() {
        var b = ch.getAttackTarget();
        if(b == null) return Optional.empty();
        return Optional.of(new Character(bot, b));
    }
}
