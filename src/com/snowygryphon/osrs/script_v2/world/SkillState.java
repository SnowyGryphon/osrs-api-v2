package com.snowygryphon.osrs.script_v2.world;

import com.snowygryphon.osrs.script.SkillData;

public class SkillState {
    private final SkillData d;

    public SkillState(SkillData d) {
        this.d = d;
    }

    public int current() {
        return d.getCurrent();
    }

    public int max() {
        return d.getMax();
    }

    public int xp() {
        return d.getXP();
    }
}
