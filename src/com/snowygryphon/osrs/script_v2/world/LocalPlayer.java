package com.snowygryphon.osrs.script_v2.world;

import com.snowygryphon.osrs.script.Bot;

public class LocalPlayer extends Player {
    private final Bot bot;
    private final com.snowygryphon.osrs.script.LocalPlayer lp;

    public LocalPlayer(Bot bot, com.snowygryphon.osrs.script.LocalPlayer ch) {
        super(bot, ch);
        this.bot = bot;
        this.lp = ch;
    }

    public Skills skills() {
        return new Skills(lp.getSkills());
    }

    public SkillState hp() {
        return skills().hp();
    }

    public int energy() {
        return lp.getEnergy();
    }

    public int specialAttack() {
        return lp.getStamina();
    }

    public SkillState prayer() {
        return skills().prayer();
    }

    public int totalLevel() {
        return lp.getTotalLevel();
    }

    public int combatLevel() {
        return lp.getCombatLevel();
    }
}
