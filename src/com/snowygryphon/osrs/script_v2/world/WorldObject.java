package com.snowygryphon.osrs.script_v2.world;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.geometry.Position;
import com.snowygryphon.osrs.script_v2.props.Actionable;
import com.snowygryphon.osrs.script_v2.props.Interactable;
import com.snowygryphon.osrs.script_v2.props.Nameable;
import com.snowygryphon.osrs.script_v2.props.Positionable;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class WorldObject extends Positionable implements Nameable, Actionable, Interactable {
    private final Bot bot;
    private final com.snowygryphon.osrs.script.WorldObject w;

    public WorldObject(Bot bot, com.snowygryphon.osrs.script.WorldObject w) {
        this.bot = bot;
        this.w = w;
    }

    public com.snowygryphon.osrs.script.WorldObject getBackend() {
        return w;
    }

    @Override
    public String name() {
        return w.getName();
    }

    @Override
    public List<String> actions() {
        return Collections.unmodifiableList(w.getActions());
    }

    @Override
    public boolean hasAction(String ... actions) {
        if(actions == null || actions.length == 0) throw new IllegalArgumentException("Action is null");
        var current_actions = actions();
        for(var a : actions) {
            if(a == null) throw new IllegalArgumentException("Action is null");
            return current_actions.contains(a);
        }
        return false;
    }

    @Override
    public Position tile() {
        int z = 0;
        if(bot.getLocalPlayer() != null) {
            z = bot.getLocalPlayer().getZ();
        }
        return new Position(w.getTile(), z);
    }

    @Override
    public void interact(String action) {
        if(action == null) throw new IllegalArgumentException("Action is null");
        bot.interactWith(w, action);
    }

    public void interact(int global_action_id, String action_name) {
        if(action_name == null) throw new IllegalArgumentException("action_name is null");
        bot.interactWith(w, global_action_id, action_name);
    }

    public boolean valid() {
        return w.isValid();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorldObject that = (WorldObject) o;
        return w.equals(that.w);
    }

    @Override
    public int hashCode() {
        return Objects.hash(w);
    }
}
