package com.snowygryphon.osrs.script_v2.spells;

import java.util.HashMap;

public enum SpellName {
    LumbridgeHomeTeleport("Lumbridge Home Teleport"),
    WindStrike("Wind Strike"),
    Confuse("Confuse"),
    EnchantCrossBowBoltOpal("Enchant Crossbow Bolt (Opal)"),
    WaterStrike("Water Strike"),
    Lvl1Enchant("Lvl-1 Enchant"),
    EnchantCrossBowBoltSapphire("Enchant Crossbow Bolt (Sapphire)"),
    EarthStrike("Earth Strike"),
    Weaken("Weaken"),
    FireStrike("Fire Strike"),
    EnchantCrossBowBoltJade("Enchant Crossbow Bolt (Jade)"),
    BonesToBananas("Bones to Bananas"),
    WindBolt("Wind Bolt"),
    Curse("Curse"),
            Bind("Bind"),
    LowLevelAlchemy("Low Level Alchemy"),
    WaterBolt("Water Bolt"),
    EnchantCrossBowBolt("Enchant Crossbow Bolt (Pearl)"),
    VarrockTeleport("Varrock Teleport"),
    Lvl2Enchant("Lvl-2 Enchant"),
    EnchantCrossBowBoltEmerald("Enchant Crossbow Bolt (Emerald)"),
    EarthBolt("Earth Bolt"),
    EnchantCrossBowBoltRedTopaz("Enchant Crossbow Bolt (Red Topaz)"),
    LumbridgeTeleport("Lumbridge Teleport"),
    TelekineticGrab("Telekinetic Grab"),
    FireBolt("Fire Bolt"),
    FaladorTeleport("Falador Teleport"),
    CrumbleUndead("Crumble Undead"),
    TeleportToHouse("Teleport to House"),
    WindBlast("Wind Blast"),
    SuperHeatItem("Superheat Item"),
    CamelotTeleport("Camelot Teleport"),
    WaterBlast("Water Blast"),
    Lvl3Enchant("Lvl-3 Enchant"),
    EnchantCrossBowBoltRuby("Enchant Crossbow Bolt (Ruby)"),
    IbanBlast("Iban Blast"),
    Snare("Snare"),
    MagicDart("Magic Dart"),
    ArdougneTeleport("Ardougne Teleport"),
    EarthBlast("Earth Blast"),
    HighLevelAlchemy("High Level Alchemy"),
    ChargeWaterOrb("Charge Water Orb"),
    Lvl4Enchant("Lvl-4 Enchant"),
    EnchantCrossBowBoltDiamond("Enchant Crossbow Bolt (Diamond)"),
    WatchtowerTeleport("Watchtower Teleport"),
    FireBlast("Fire Blast"),
    ChargeEarthOrb("Charge Earth Orb"),
    BonesToPeaches("Bones to Peaches"),
    SaradominStrike("Saradomin Strike"),
    FlamesOfZamorak("Flames of Zamorak"),
    ClawsOfGuthix("Claws of Guthix"),
    TrollHeimTeleport("Trollheim Teleport"),
    WindWave("Wind Wave"),
    ChargeFireOrb("Charge Fire Orb"),
    ApeAtollTeleport("Ape Atoll Teleport"),
    WaterWave("Water Wave"),
    ChargeAirOrb("Charge Air Orb"),
            Vulnerability("Vuln0erability"),
    Lvl5Enchant("Lvl-5 Enchant"),
    EnchantCrossBowBoltDragonStone("Enchant Crossbow Bolt (Dragonstone)"),
    KourendCastleTeleport("Kourend Castle Teleport"),
    EarthWave("Earth Wave"),
    Enfeeble("Enfeeble"),
    TeleotherLumbridge("Teleother Lumbridge"),
    FireWave("Fire Wave"),
    Entangle("Entangle"),
            Stun("Stun"),
    Charge("Charge"),
    WindSurge("Wind Surge"),
    TeleotherFalador("Teleother Falador"),
    WaterSurge("Water Surge"),
    TeleBlock("Tele Block"),
    TeleportToTarget("Teleport to Target"),
    Lvl6Enchant("Lvl-6 Enchant"),
    EnchantCrossBowBoltOnyx("Enchant Crossbow Bolt (Onyx)"),
    TeleotherCamelot("Teleother Camelot"),
    EarthSurge("Earth Surge"),
    Lvl7Enchant("Lvl-7 Enchant"),
    FireSurge("Fire Surge");

    public final String name;
    public static final HashMap<String, SpellName> NAME_ENUM_LOOKUP = create_lookup();

    private static HashMap<String, SpellName> create_lookup() {
        var r = new HashMap<String, SpellName>();
        for(SpellName n : SpellName.values()) {
            r.put(n.name, n);
        }
        return r;
    }

    private SpellName(String n) {
        this.name = n;
    }
}
