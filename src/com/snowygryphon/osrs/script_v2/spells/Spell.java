package com.snowygryphon.osrs.script_v2.spells;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.SpellData;
import com.snowygryphon.osrs.script_v2.world.item.InventoryItem;
import com.snowygryphon.osrs.script_v2.world.WorldObject;

public class Spell {
    private final SpellName name;
    private final SpellData s;
    private final Bot bot;

    public Spell(SpellName name, SpellData s, Bot bot) {
        this.name = name;
        this.s = s;
        this.bot = bot;
    }

    public SpellName name() { return name; }

    public void select() {
        bot.getSpells().select(s);
    }

    public void cast() {
        bot.getSpells().castSpell(s);
    }

    public void castOn(InventoryItem item) {
        bot.getSpells().castSpellOn(s, item.getBackend());
    }

    public void castOn(WorldObject obj) {
        bot.getSpells().castSpellOn(s, obj.getBackend());
    }
}
