package com.snowygryphon.osrs.script_v2.screen;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.MobileKeyCode;
import org.joml.Vector2i;

public class Screen {
    private final Bot bot;

    public Screen(Bot bot) {
        this.bot = bot;
    }

    public void tap(Vector2i p) {
        if(p == null) throw new IllegalArgumentException("p is null");
        bot.tap(p);
    }

    public void tap(int x, int y) {
        bot.tap(new Vector2i(x, y));
    }

    public void click(MobileKeyCode c) {
        if(c == null) throw new IllegalArgumentException("c is null");
        bot.sendKeyCode(c);
    }

    public void swipe(int from_x, int from_y, int to_x, int to_y) {
        bot.swipe(new Vector2i(from_x, from_y), new Vector2i(to_x, to_y));
    }

    public void swipe(Vector2i from, Vector2i to) {
        if(from == null) throw new IllegalArgumentException("from is null");
        if(to == null) throw new IllegalArgumentException("to is null");
        bot.swipe(from, to);
    }


}
