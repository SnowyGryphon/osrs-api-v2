package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.item.InventoryItem;

import java.util.Collection;
import java.util.Optional;

public class InventoryStream extends VirtualItemStream<InventoryStream, InventoryItem> {
    public InventoryStream(Collection<InventoryItem> it, Bot bot) {
        super(it, bot);
    }

    public InventoryStream id(int ... ids) {
        return filter(i -> Util.any(i.id(), ids));
    }

    public InventoryStream count(int c) {
        return filter(i -> i.count() == c);
    }

    public InventoryStream minCount(int c) {
        return filter(i -> i.count() >= c);
    }

    public InventoryStream maxCount(int c) {
        return filter(i -> i.count() <= c);
    }

    public Optional<InventoryItem> slot(int s) {
        return filter(i -> i.slotID() == s).first();
    }

    public InventoryGenericStream genericStream() {
        return new InventoryGenericStream(bot, data);
    }
}
