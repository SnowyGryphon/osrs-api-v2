package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.NPC;

import java.util.ArrayList;

public class NPCGenericStream extends CharacterGenericStream<NPC> {
    public NPCGenericStream(Bot bot, ArrayList<NPC> data) {
        super(bot, data);
    }
}
