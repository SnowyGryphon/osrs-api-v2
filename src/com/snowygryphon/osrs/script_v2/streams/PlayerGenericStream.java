package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.EquipmentSlot;
import com.snowygryphon.osrs.script_v2.world.Player;

import java.util.ArrayList;

public class PlayerGenericStream extends CharacterGenericStream<Player> {
    public PlayerGenericStream(Bot bot, ArrayList<Player> data) {
        super(bot, data);
    }

    public void wearsItem(EquipmentSlot slot) {
        if(slot == null) throw new IllegalArgumentException("slot is null");
        filter(p -> p.equipment().containsKey(slot));
    }

    private static boolean wearsItemImpl(Player player, String ... names) {
        for(var item : player.equipment().values()) {
            if(Util.any(item.name(),names)) return true;
        }
        return false;
    }

    private static boolean wearsItemImpl(Player player, int ... ids) {
        for(var item : player.equipment().values()) {
            if(Util.any(item.id(), ids)) return true;
        }
        return false;
    }

    public void wearsItem(String ... names) {
        filter(p -> wearsItemImpl(p, names));
    }

    public void wearsItem(int ... item_ids) {
        filter(p -> wearsItemImpl(p, item_ids));
    }
}
