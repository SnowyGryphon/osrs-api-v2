package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.streams.props.ItemStream;
import com.snowygryphon.osrs.script_v2.world.item.Item;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class VirtualItemGenericStream<T extends Item> extends GenericStreamImpl<T>
    implements ItemStream
{
    public VirtualItemGenericStream(Bot bot, ArrayList<T> data) {
        super(bot, data);
    }

    public void name(String ... names) {
        Util.enforceNotNull("name is null", names);
        filter(o -> Util.any(o.name(), names));
    }

    public void nameStarts(String ... names) {
        Util.enforceNotNull("name is null", names);
        filter((o -> Util.anyStartsWith(o.name(), names)));
    }

    public void nameEnds(String ... names) {
        Util.enforceNotNull("name is null", names);
        filter((o -> Util.anyEndsWith(o.name(), names)));
    }

    public void nameContains(String ... names) {
        Util.enforceNotNull("name is null", names);
        filter(o -> Util.anyContains(o.name(), names));
    }

    public void nameRegex(Pattern... patterns) {
        Util.enforceNotNull("pattern is null", patterns);
        filter(o -> Util.anyMatch(o.name(), patterns));
    }

    public void nameRegex(String ... str_patterns) {
        Util.enforceNotNull("pattern is null", str_patterns);
        var patterns = new Pattern[str_patterns.length];
        for(int i = 0; i < patterns.length; ++i) {
            patterns[i] = Pattern.compile(str_patterns[i]);
        }
        nameRegex(patterns);
    }

    public void action(String ... actions) {
        Util.enforceNotNull("action is null", actions);
        filter(o -> {
            var actual_actions = o.actions();
            for(String a : actions) {
                if(actual_actions.contains(a)) return true;
            }
            return false;
        });
    }
}
