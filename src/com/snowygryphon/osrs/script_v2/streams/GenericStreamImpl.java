package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

// Define all the stream properties (except item for now)
// Implement generic stream function for each concrete stream type
public class GenericStreamImpl<T> implements GenericStream<T> {
    private final Bot bot;
    private final ArrayList<T> data;
    private boolean invert_next_filter_call;

    public GenericStreamImpl(Bot bot, ArrayList<T> data) {
        this.bot = bot;
        this.data = new ArrayList<>(data);
    }

    public void not() {
        invert_next_filter_call = true;
    }

    public void filter(FilterCB<T> cb) {
        if(invert_next_filter_call) {
            data.removeIf(cb::filterIn);
            invert_next_filter_call = false;
        } else {
            data.removeIf(e -> !cb.filterIn(e));
        }
    }

    public void sort(Comparator<T> cb) {
        data.sort(cb);
    }

    public Stream<T> stdStream() {
        return data.stream();
    }

    public <R> Stream<R> map(Function<T, R> cb) {
        return stdStream().map(cb);
    }


    public List<T> toList() {
        return new ArrayList<>(data);
    }

    public Optional<T> first() {
        if(data.size() > 0) return Optional.ofNullable(data.get(0));
        return Optional.empty();
    }

    public Optional<T> last() {
        if(data.size() > 0) return Optional.ofNullable(data.get(data.size() - 1));
        return Optional.empty();
    }

    public Optional<T> random() {
        if(data.size() > 0) return Optional.ofNullable(data.get(bot.getRandom().nextInt(data.size())));
        return Optional.empty();
    }
}
