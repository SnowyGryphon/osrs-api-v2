package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.geometry.Area;
import com.snowygryphon.osrs.script_v2.props.Positionable;
import com.snowygryphon.osrs.script_v2.streams.props.ActionableStream;
import com.snowygryphon.osrs.script_v2.world.WorldObject;
import com.snowygryphon.osrs.script_v2.streams.props.NameableStream;
import com.snowygryphon.osrs.script_v2.streams.props.PositionableStream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.regex.Pattern;

public abstract class WorldObjectGenericStream<T extends WorldObject> extends GenericStreamImpl<T>
    implements NameableStream,
        ActionableStream,
        PositionableStream<T>
{
    public WorldObjectGenericStream(Bot bot, ArrayList<T> data) {
        super(bot, data);
    }

    public void name(String ... names) {
        Util.enforceNotNull("name is null", names);
        filter(o -> Util.any(o.name(), names));
    }

    public void nameStarts(String ... names) {
        Util.enforceNotNull("name is null", names);
        filter((o -> Util.anyStartsWith(o.name(), names)));
    }

    public void nameEnds(String ... names) {
        Util.enforceNotNull("name is null", names);
        filter((o -> Util.anyEndsWith(o.name(), names)));
    }

    public void nameContains(String ... names) {
        Util.enforceNotNull("name is null", names);
        filter(o -> Util.anyContains(o.name(), names));
    }

    public void nameRegex(Pattern ... patterns) {
        Util.enforceNotNull("pattern is null", patterns);
        filter(o -> Util.anyMatch(o.name(), patterns));
    }

    public void nameRegex(String ... str_patterns) {
        Util.enforceNotNull("pattern is null", str_patterns);
        var patterns = new Pattern[str_patterns.length];
        for(int i = 0; i < patterns.length; ++i) {
            patterns[i] = Pattern.compile(str_patterns[i]);
        }
        nameRegex(patterns);
    }

    public void action(String ... actions) {
        Util.enforceNotNull("action is null", actions);
        filter(o -> {
            var actual_actions = o.actions();
            for(String a : actions) {
                if(actual_actions.contains(a)) return true;
            }
            return false;
        });
    }

    public void within(Area area) {
        if(area == null) throw new IllegalArgumentException("area is null");
        filter(o -> o.within(area));
    }

    public void withinCircle(Positionable center, double radius) {
        if(center == null) throw new IllegalArgumentException("center is null");
        within(new Area().addCircle(center, radius));
    }

    public void withinBox(Positionable bottom_left, Positionable top_right) {
        if(bottom_left == null) throw new IllegalArgumentException("bottom_left is null");
        if(top_right == null) throw new IllegalArgumentException("top_right is null");
        within(new Area().addBox(bottom_left,top_right));
    }

    public Optional<T> closestTo(Positionable p) {
        if(p == null) throw new IllegalArgumentException("p is null");
        sort(Comparator.comparingDouble(a -> a.distanceTo(p)));
        return first();
    }

    public Optional<T> closest() {
        return com.snowygryphon.osrs.script_v2.Bot.threadlocal().localPlayer().map(this::closestTo).get();
    }

    public Optional<T> furthestTo(Positionable p) {
        if(p == null) throw new IllegalArgumentException("p is null");
        var x = Comparator.<T>comparingDouble(a -> a.distanceTo(p));
        sort(x.reversed());
        return first();
    }

    public Optional<T> furthest() {
        return com.snowygryphon.osrs.script_v2.Bot.threadlocal().localPlayer().map(this::furthestTo).get();
    }
}
