package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.GroundObject;

import java.util.Collection;

public class GroundObjectStream extends WorldObjectStream<GroundObjectStream, GroundObject> {
    public GroundObjectStream(Collection<GroundObject> it, Bot bot) {
        super(it, bot);
    }

    public GroundObjectStream id(int ... ids) {
        return filter(o -> Util.any(o.id(), ids));
    }

    public GroundObjectGenericStream genericStream() {
        return new GroundObjectGenericStream(bot, data);
    }
}
