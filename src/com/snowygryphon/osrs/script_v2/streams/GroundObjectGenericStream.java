package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.streams.props.IDableStream;
import com.snowygryphon.osrs.script_v2.world.GroundObject;

import java.util.ArrayList;

public class GroundObjectGenericStream extends WorldObjectGenericStream<GroundObject>
    implements IDableStream
{
    public GroundObjectGenericStream(Bot bot, ArrayList<GroundObject> data) {
        super(bot, data);
    }

    public void id(int ... ids) {
        filter(o -> Util.any(o.id(), ids));
    }
}
