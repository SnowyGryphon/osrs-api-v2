package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.streams.props.AnimatableStream;
import com.snowygryphon.osrs.script_v2.world.Character;
import com.snowygryphon.osrs.script_v2.streams.props.MovableStream;
import com.snowygryphon.osrs.script_v2.streams.props.TargetableStream;

import java.util.ArrayList;

public abstract class CharacterGenericStream<T extends Character> extends WorldObjectGenericStream<T>
    implements AnimatableStream,
        MovableStream,
        TargetableStream
{
    public CharacterGenericStream(Bot bot, ArrayList<T> data) {
        super(bot, data);
    }

    public void anim(int ... anims) {
        filter(c -> Util.any(c.anim(), anims));
    }

    public void moving(boolean m) {
        filter(c -> c.moving() == m);
    }

    public void target(Character ... chars) {
        Util.enforceNotNull("char is null", chars);
        filter(c -> c.target().map(t -> Util.any(t, chars)).orElse(false));
    }
}
