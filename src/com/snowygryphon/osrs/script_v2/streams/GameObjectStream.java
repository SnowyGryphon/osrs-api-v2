package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.GameObject;

import java.util.Collection;

public class GameObjectStream extends WorldObjectStream<GameObjectStream, GameObject> {
    public GameObjectStream(Collection<GameObject> it, Bot bot) {
        super(it, bot);
    }

    public GameObjectStream id(int ... ids) {
        return filter(o -> Util.any(o.id(), ids));
    }

    public GameObjectGenericStream genericStream() {
        return new GameObjectGenericStream(bot, data);
    }
}
