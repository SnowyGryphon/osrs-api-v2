package com.snowygryphon.osrs.script_v2.streams;

import java.util.regex.Pattern;

class Util {
    static void enforceNotNull(String msg, Object[] objects) {
        for(Object o : objects) if(o == null) throw new IllegalArgumentException(msg);
    }

    static <T> boolean any(T v, T ... candidates) {
        for(T c : candidates) if(v.equals(c)) return true;
        return false;
    }

    static boolean any(int v, int[] candidates) {
        for(int c : candidates) if(v == c) return true;
        return false;
    }

    static boolean anyStartsWith(String v, String ... candidates) {
        for(String c : candidates) if(v.startsWith(c)) return true;
        return false;
    }

    static boolean anyEndsWith(String v, String ... candidates) {
        for(String c : candidates) if(v.endsWith(c)) return true;
        return false;
    }

    static boolean anyContains(String v, String ... candidates) {
        for(String c : candidates) if(v.contains(c)) return true;
        return false;
    }

    static boolean anyMatch(String v, Pattern... candidates) {
        for(Pattern p : candidates) {
            var m = p.matcher(v);
            if(m.find()) return true;
        }
        return false;
    }
}
