package com.snowygryphon.osrs.script_v2.streams;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public interface GenericStream<T> {
    void not();
    void filter(FilterCB<T> cb);
    void sort(Comparator<T> cb);
    Stream<T> stdStream();

    <R> Stream<R> map(Function<T, R> cb);
    List<T> toList();
    Optional<T> first();
    Optional<T> last();
    Optional<T> random();
}
