package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;

import java.util.*;

class StreamBase<PARENT_T extends StreamBase, T> {
    protected final Bot bot;
    protected final ArrayList<T> data;
    private boolean invert_next_filter_call;

    public StreamBase(Collection<T> it, Bot bot) {
        this.bot = bot;
        data = new ArrayList<>();
        data.addAll(it);
    }



    // TODO: toList
    // TODO: map

    public PARENT_T not() {
        invert_next_filter_call = true;
        return (PARENT_T)this;
    }

    public PARENT_T filter(FilterCB<T> cb) {
        if(invert_next_filter_call) {
            data.removeIf(cb::filterIn);
            invert_next_filter_call = false;
        } else {
            data.removeIf(e -> !cb.filterIn(e));
        }

        return (PARENT_T)this;
    }

    public PARENT_T sort(Comparator<T> cb) {
        data.sort(cb);
        return (PARENT_T)this;
    }

    public Optional<T> first() {
        if(data.size() > 0) return Optional.ofNullable(data.get(0));
        return Optional.empty();
    }

    public Optional<T> last() {
        if(data.size() > 0) return Optional.ofNullable(data.get(data.size() - 1));
        return Optional.empty();
    }

    public Optional<T> random() {
        if(data.size() > 0) return Optional.ofNullable(data.get(bot.getRandom().nextInt(data.size())));
        return Optional.empty();
    }

    public java.util.stream.Stream<T> stdStream() {
        return new ArrayList<>(data).stream();
    }
}
