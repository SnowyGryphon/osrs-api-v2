package com.snowygryphon.osrs.script_v2.streams;

public interface FilterCB<T> {
    boolean filterIn(T data);
}
