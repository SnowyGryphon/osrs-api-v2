package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.item.BankItem;

import java.util.Collection;

public class BankStream extends VirtualItemStream<BankStream, BankItem> {
    public BankStream(Collection<BankItem> it, Bot bot) {
        super(it, bot);
    }

    public BankStream count(int c) {
        return filter(i -> i.count() == c);
    }

    public BankStream minCount(int c) {
        return filter(i -> i.count() >= c);
    }

    public BankStream maxCount(int c) {
        return filter(i -> i.count() <= c);
    }

    public BankGenericStream genericStream() {
        return new BankGenericStream(bot, data);
    }
}
