package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.NPC;

import java.util.Collection;

public class NPCStream extends CharacterStream<NPCStream, NPC> {
    public NPCStream(Collection<NPC> it, Bot bot) {
        super(it, bot);
    }

    public NPCGenericStream genericStream() {
        return new NPCGenericStream(bot, data);
    }
}
