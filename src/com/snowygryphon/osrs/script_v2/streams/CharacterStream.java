package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.Character;

import java.util.Collection;

public abstract class CharacterStream<PARENT_T extends StreamBase, T extends Character> extends WorldObjectStream<PARENT_T, T> {
    public CharacterStream(Collection<T> it, Bot bot) {
        super(it, bot);
    }

    public PARENT_T anim(int ... anims) {
        return filter(c -> Util.any(c.anim(), anims));
    }

    public PARENT_T moving(boolean m) {
        return filter(c -> c.moving() == m);
    }

    public PARENT_T target(Character ... chars) {
        Util.enforceNotNull("char is null", chars);
        return filter(c -> c.target().map(t -> Util.any(t, chars)).orElse(false));
    }

    public PARENT_T isAnimating() {
        return filter(Character::isAnimating);
    }
}
