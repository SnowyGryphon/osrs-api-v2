package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.EquipmentSlot;
import com.snowygryphon.osrs.script_v2.world.Player;

import java.util.Collection;

public class PlayerStream extends CharacterStream<PlayerStream, Player> {
    public PlayerStream(Collection<Player> it, Bot bot) {
        super(it, bot);
    }

    public PlayerGenericStream genericStream() {
        return new PlayerGenericStream(bot, data);
    }

    public PlayerStream skulled() {
        return filter(Player::skulled);
    }

    public PlayerStream skullIcon(int ... icons) {
        return filter(p -> Util.any(icons, p.skullIcon()));
    }

    public PlayerStream hasOverHeadIcon() {
        return filter(Player::hasOverHeadIcon);
    }

    public PlayerStream overHeadIcon(int ... icons) {
        return filter(p -> Util.any(icons, p.overHeadIcon()));
    }

    public PlayerStream wearsItem(EquipmentSlot slot) {
        if(slot == null) throw new IllegalArgumentException("slot is null");
        return filter(p -> p.equipment().containsKey(slot));
    }

    private static boolean wearsItemImpl(Player player, String ... names) {
        for(var item : player.equipment().values()) {
            if(Util.any(item.name(),names)) return true;
        }
        return false;
    }

    private static boolean wearsItemImpl(Player player, int ... ids) {
        for(var item : player.equipment().values()) {
            if(Util.any(item.id(), ids)) return true;
        }
        return false;
    }

    public PlayerStream wearsItem(String ... names) {
        return filter(p -> wearsItemImpl(p, names));
    }

    public PlayerStream wearsItem(int ... item_ids) {
        return filter(p -> wearsItemImpl(p, item_ids));
    }
}
