package com.snowygryphon.osrs.script_v2.streams.props;

import java.util.regex.Pattern;

public interface NameableStream {
    void name(String ... names);
    void nameStarts(String ... names);
    void nameEnds(String ... names);
    void nameContains(String ... names);
    void nameRegex(Pattern ... patterns);
    void nameRegex(String ... patterns);
}
