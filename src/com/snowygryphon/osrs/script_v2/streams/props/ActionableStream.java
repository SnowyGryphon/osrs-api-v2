package com.snowygryphon.osrs.script_v2.streams.props;

import java.util.List;

public interface ActionableStream {
    void action(String ... actions);
}
