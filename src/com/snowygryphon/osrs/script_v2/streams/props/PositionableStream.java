package com.snowygryphon.osrs.script_v2.streams.props;

import com.snowygryphon.osrs.script_v2.geometry.Area;
import com.snowygryphon.osrs.script_v2.props.Positionable;
import com.snowygryphon.osrs.script_v2.world.WorldObject;

public interface PositionableStream<T extends WorldObject> {
    void within(Area area);
    void withinCircle(Positionable p, double radius);
    void withinBox(Positionable bottom_left, Positionable top_right);


}
