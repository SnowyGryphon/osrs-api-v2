package com.snowygryphon.osrs.script_v2.streams.props;

import com.snowygryphon.osrs.script_v2.world.Character;

public interface TargetableStream {
    void target(Character... chars);
}
