package com.snowygryphon.osrs.script_v2.streams.props;

public interface MovableStream {
    void moving(boolean m);
}
