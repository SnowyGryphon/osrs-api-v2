package com.snowygryphon.osrs.script_v2.streams.props;

public interface AnimatableStream {
    void anim(int ... ids);
}
