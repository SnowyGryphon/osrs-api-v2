package com.snowygryphon.osrs.script_v2.streams.props;

public interface IDableStream {
    void id(int ... ids);
}
