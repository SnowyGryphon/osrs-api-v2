package com.snowygryphon.osrs.script_v2.streams.props;

public interface CountableStream {
    void count(int c);
    void minCount(int c);
    void maxCount(int c);
}
