package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.streams.props.CountableStream;
import com.snowygryphon.osrs.script_v2.world.item.EquipmentItem;
import com.snowygryphon.osrs.script_v2.streams.props.IDableStream;

import java.util.ArrayList;
import java.util.Optional;

public class EquipmentGenericStream extends VirtualItemGenericStream<EquipmentItem>
    implements IDableStream,
        CountableStream
{
    public EquipmentGenericStream(Bot bot, ArrayList<EquipmentItem> data) {
        super(bot, data);
    }

    public void id(int ... ids) {
        filter(i -> Util.any(i.id(), ids));
    }

    public void count(int c) {
        filter(i -> i.count() == c);
    }

    public void minCount(int c) {
        filter(i -> i.count() >= c);
    }

    public void maxCount(int c) {
        filter(i -> i.count() <= c);
    }

    public Optional<EquipmentItem> slot(int s) {
        filter(i -> i.slotID() == s);
        return first();
    }
}
