package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.streams.props.CountableStream;
import com.snowygryphon.osrs.script_v2.world.item.BankItem;

import java.util.ArrayList;

public class BankGenericStream extends VirtualItemGenericStream<BankItem>
    implements CountableStream
{
    public BankGenericStream(Bot bot, ArrayList<BankItem> data) {
        super(bot, data);
    }

    public void count(int c) {
        filter(i -> i.count() == c);
    }

    public void minCount(int c) {
        filter(i -> i.count() >= c);
    }

    public void maxCount(int c) {
        filter(i -> i.count() <= c);
    }
}
