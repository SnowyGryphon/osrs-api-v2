package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.geometry.Area;
import com.snowygryphon.osrs.script_v2.props.Positionable;
import com.snowygryphon.osrs.script_v2.world.WorldObject;

import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.regex.Pattern;

public abstract class WorldObjectStream<PARENT_T extends StreamBase, T extends WorldObject> extends StreamBase<PARENT_T, T> {
    public WorldObjectStream(Collection<T> it, Bot bot) {
        super(it, bot);
    }



    public PARENT_T name(String ... names) {
        Util.enforceNotNull("name is null", names);
        return filter(o -> Util.any(o.name(), names));
    }

    public PARENT_T nameStarts(String ... names) {
        Util.enforceNotNull("name is null", names);
        return filter((o -> Util.anyStartsWith(o.name(), names)));
    }

    public PARENT_T nameEnds(String ... names) {
        Util.enforceNotNull("name is null", names);
        return filter((o -> Util.anyEndsWith(o.name(), names)));
    }

    public PARENT_T nameContains(String ... names) {
        Util.enforceNotNull("name is null", names);
        return filter(o -> Util.anyContains(o.name(), names));
    }

    public PARENT_T nameRegex(Pattern ... patterns) {
        Util.enforceNotNull("pattern is null", patterns);
        return filter(o -> Util.anyMatch(o.name(), patterns));
    }

    public PARENT_T nameRegex(String ... str_patterns) {
        Util.enforceNotNull("pattern is null", str_patterns);
        var patterns = new Pattern[str_patterns.length];
        for(int i = 0; i < patterns.length; ++i) {
            patterns[i] = Pattern.compile(str_patterns[i]);
        }
        return nameRegex(patterns);
    }

    public PARENT_T action(String ... actions) {
        Util.enforceNotNull("action is null", actions);
        return filter(o -> {
            var actual_actions = o.actions();
            for(String a : actions) {
                if(actual_actions.contains(a)) return true;
            }
            return false;
        });
    }

    public Optional<T> closestTo(Positionable p) {
        if(p == null) throw new IllegalArgumentException("p is null");
        return sort(Comparator.comparingDouble(a -> a.distanceTo(p))).first();
    }

    public Optional<T> closest() {
        return com.snowygryphon.osrs.script_v2.Bot.threadlocal().localPlayer().map(this::closestTo).get();
    }

    public Optional<T> furthestTo(Positionable p) {
        if(p == null) throw new IllegalArgumentException("p is null");
        var x = Comparator.<T>comparingDouble(a -> a.distanceTo(p));
        return sort(x.reversed()).first();
    }

    public Optional<T> furthest() {
        return com.snowygryphon.osrs.script_v2.Bot.threadlocal().localPlayer().map(this::furthestTo).get();
    }

    public PARENT_T within(Area area) {
        if(area == null) throw new IllegalArgumentException("area is null");
        return filter(o -> o.within(area));
    }

    public PARENT_T withinCircle(Positionable center, double radius) {
        if(center == null) throw new IllegalArgumentException("center is null");
        return within(new Area().addCircle(center, radius));
    }

    public PARENT_T withinBox(Positionable bottom_left, Positionable top_right) {
        if(bottom_left == null) throw new IllegalArgumentException("bottom_left is null");
        if(top_right == null) throw new IllegalArgumentException("top_right is null");
        return within(new Area().addBox(bottom_left,top_right));
    }
}
