package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.streams.props.IDableStream;
import com.snowygryphon.osrs.script_v2.world.GameObject;

import java.util.ArrayList;

public class GameObjectGenericStream extends WorldObjectGenericStream<GameObject>
    implements IDableStream
{
    public GameObjectGenericStream(Bot bot, ArrayList<GameObject> data) {
        super(bot, data);
    }

    public void id(int ... ids) {
        filter(o -> Util.any(o.id(), ids));
    }
}
