package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.item.EquipmentItem;

import java.util.Collection;
import java.util.Optional;

public class EquipmentStream extends VirtualItemStream<EquipmentStream, EquipmentItem> {
    public EquipmentStream(Collection<EquipmentItem> it, Bot bot) {
        super(it, bot);
    }

    public EquipmentStream id(int ... ids) {
        return filter(i -> Util.any(i.id(), ids));
    }

    public EquipmentStream count(int c) {
        return filter(i -> i.count() == c);
    }

    public EquipmentStream minCount(int c) {
        return filter(i -> i.count() >= c);
    }

    public EquipmentStream maxCount(int c) {
        return filter(i -> i.count() <= c);
    }

    public Optional<EquipmentItem> slot(int s) {
        return filter(i -> i.slotID() == s).first();
    }

    public EquipmentGenericStream genericStream() {
        return new EquipmentGenericStream(bot, data);
    }
}
