package com.snowygryphon.osrs.script_v2.streams;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.item.Item;

import java.util.Collection;
import java.util.regex.Pattern;

public abstract class VirtualItemStream<PARENT_T extends VirtualItemStream, T extends Item> extends StreamBase<PARENT_T, T> {
    public VirtualItemStream(Collection<T> it, Bot bot) {
        super(it, bot);
    }

    public PARENT_T name(String ... names) {
        Util.enforceNotNull("name is null", names);
        return filter(o -> Util.any(o.name(), names));
    }

    public PARENT_T nameStarts(String ... names) {
        Util.enforceNotNull("name is null", names);
        return filter((o -> Util.anyStartsWith(o.name(), names)));
    }

    public PARENT_T nameEnds(String ... names) {
        Util.enforceNotNull("name is null", names);
        return filter((o -> Util.anyEndsWith(o.name(), names)));
    }

    public PARENT_T nameContains(String ... names) {
        Util.enforceNotNull("name is null", names);
        return filter(o -> Util.anyContains(o.name(), names));
    }

    public PARENT_T nameRegex(Pattern... patterns) {
        Util.enforceNotNull("pattern is null", patterns);
        return filter(o -> Util.anyMatch(o.name(), patterns));
    }

    public PARENT_T nameRegex(String ... str_patterns) {
        Util.enforceNotNull("pattern is null", str_patterns);
        var patterns = new Pattern[str_patterns.length];
        for(int i = 0; i < patterns.length; ++i) {
            patterns[i] = Pattern.compile(str_patterns[i]);
        }
        return nameRegex(patterns);
    }

    public PARENT_T action(String ... actions) {
        Util.enforceNotNull("action is null", actions);
        return filter(o -> {
            var actual_actions = o.actions();
            for(String a : actions) {
                if(actual_actions.contains(a)) return true;
            }
            return false;
        });
    }
}
