package com.snowygryphon.osrs.script_v2.stores;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.prayer.Prayer;
import com.snowygryphon.osrs.script_v2.prayer.PrayerName;

import java.util.HashMap;
import java.util.Optional;

public class Prayers {
    private final Bot bot;

    public Prayers(Bot bot) {
        this.bot = bot;
    }

    public HashMap<PrayerName, Prayer> all() {
        var r = new HashMap<PrayerName, Prayer>();
        for(var e : bot.getPrayer().getPrayers().entrySet()) {
            var n = PrayerName.NAME_ENUM_LOOKUP.get(e.getKey());
            r.put(n, new Prayer(bot, e.getValue(), n));
        }
        return r;
    }

    private Optional<Prayer> try_get(PrayerName n) {
        return Optional.ofNullable(all().get(n));
    }

    public void activate(PrayerName n) {
        if(n == null) throw new IllegalArgumentException("n is null");
        try_get(n).ifPresent(Prayer::activate);
    }

    public void activateFast(PrayerName n) {
        if(n == null) throw new IllegalArgumentException("n is null");
        try_get(n).ifPresent(Prayer::activateFast);
    }

    public void deactivate(PrayerName n) {
        if(n == null) throw new IllegalArgumentException("n is null");
        try_get(n).ifPresent(Prayer::deactivate);
    }

    public void deactivateFast(PrayerName n) {
        if(n == null) throw new IllegalArgumentException("n is null");
        try_get(n).ifPresent(Prayer::deactivateFast);
    }

    public void isActive(PrayerName n) {
        if(n == null) throw new IllegalArgumentException("n is null");
        try_get(n).ifPresent(Prayer::isActive);
    }

    public boolean canActivate(PrayerName n) {
        if(n == null) throw new IllegalArgumentException("n is null");
        return try_get(n).map(Prayer::canActivate).orElse(false);
    }
}
