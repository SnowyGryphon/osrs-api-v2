package com.snowygryphon.osrs.script_v2.stores;

import com.snowygryphon.osrs.script.BankLocation;
import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.bank.Bank;
import com.snowygryphon.osrs.script_v2.streams.BankStream;
import com.snowygryphon.osrs.script_v2.world.item.BankItem;
import com.snowygryphon.osrs.script_v2.world.item.InventoryItem;

import java.util.Optional;
import java.util.stream.Collectors;

public class Banking {
    private final Bot bot;
    private final Inventory inventoryItems;

    public Banking(Bot bot, Inventory inventoryItems) {
        this.bot = bot;
        this.inventoryItems = inventoryItems;
    }

    public BankStream items() {
        return new BankStream(
                bot.getBank().getItems().stream().map(i -> new BankItem(bot, i)).collect(Collectors.toList()), bot
        );
    }

    public boolean contains(String ... names) {
        return items().name(names).first().isPresent();
    }

    public boolean withdraw(BankItem item, int count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        switch(count) {
            case 1:
                return bot.getBank().withdrawOne(item.name());
            case 5:
                return bot.getBank().withdrawFive(item.name());
            default:
                return bot.getBank().withdrawX(item.name(), count);
        }
    }

    public boolean withdraw(String item_name, int count) {
        var item = items().name(item_name).first();
        if(item.isPresent()) {
            return withdraw(item.get(), count);
        }
        return false;
    }

    public boolean withdrawAll(BankItem item) {
        if(item == null) throw new IllegalArgumentException("item is null");
        return bot.getBank().withdrawAll(item.name());
    }

    public boolean withdrawAll(String ... items) {
        var item = items().name(items).first();
        if(item.isPresent()) {
            return bot.getBank().withdrawAll(item.get().name());
        }
        return false;
    }

    public boolean deposit(InventoryItem item, int count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        switch(count) {
            case 1: return bot.getBank().depositOne(item.name());
            case 5: return bot.getBank().depositFive(item.name());
            case 10: return bot.getBank().depositTen(item.name());
            default: return bot.getBank().depositX(item.name(), count);
        }
    }

    public boolean deposit(String item_name, int count) {
        if(item_name == null) throw new IllegalArgumentException("item_name is null");
        var item = inventoryItems.random(item_name);
        if(item.isPresent()) {
            return deposit(item.get(), count);
        }
        return false;
    }

    public boolean depositAll(InventoryItem item) {
        if(item == null) throw new IllegalArgumentException("item is null");
        return bot.getBank().depositAll(item.name());
    }

    public boolean depositAll(String ... item_name) {
        var item = inventoryItems.random(item_name);
        if(item.isPresent()) {
            return depositAll(item.get());
        }
        return false;
    }

    public boolean depositInventory() {
        return bot.getBank().depositInventory();
    }

    public boolean depositEquipment() {
        return bot.getBank().depositEquipment();
    }

    public Optional<com.snowygryphon.osrs.script_v2.bank.BankLocation> closest() {
        var loc = bot.getBank().getClosestBank();
        if(loc == null) return Optional.empty();
        return Optional.of(com.snowygryphon.osrs.script_v2.bank.BankLocation.V1_V2_LOOKUP.get(loc));
    }

    public boolean openBank(Bank loc) {
        if(loc == null) throw new IllegalArgumentException("loc is null");
        return bot.getBank().openBank(loc.backend);
    }

    public boolean openBank(BankLocation bank) {
        if(bank == null) throw new IllegalArgumentException("bank is null");
        return openBank(new Bank(bank));
    }

    public boolean openBank(com.snowygryphon.osrs.script_v2.bank.BankLocation bank) {
        return openBank(bank.bank);
    }

    public boolean openClosest() {
        return bot.getBank().openClosestBank();
    }
}
