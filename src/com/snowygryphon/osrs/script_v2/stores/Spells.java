package com.snowygryphon.osrs.script_v2.stores;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script_v2.world.item.InventoryItem;
import com.snowygryphon.osrs.script_v2.spells.Spell;
import com.snowygryphon.osrs.script_v2.spells.SpellName;
import com.snowygryphon.osrs.script_v2.world.WorldObject;

import java.util.HashMap;
import java.util.Optional;

public class Spells {
    private final Bot bot;

    public Spells(Bot bot) {
        this.bot = bot;
    }

    public HashMap<SpellName, Spell> all() {
        var r = new HashMap<SpellName, Spell>();
        for(var e : bot.getSpells().getSpells().entrySet()) {
            var n = SpellName.NAME_ENUM_LOOKUP.get(e.getKey());
            r.put(n, new Spell(n, e.getValue(), bot));
        }
        return r;
    }

    public Spell get(SpellName n) {
        return all().get(n);
    }

    public void cast(SpellName n) {
        Optional.ofNullable(get(n)).ifPresent(Spell::cast);
    }

    public void castOn(SpellName n, InventoryItem item) {
        Optional.ofNullable(get(n)).ifPresent(s -> s.castOn(item));
    }

    public void castOn(SpellName n, WorldObject obj) {
        Optional.ofNullable(get(n)).ifPresent(s -> s.castOn(obj));
    }
}
