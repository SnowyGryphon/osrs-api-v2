package com.snowygryphon.osrs.script_v2.stores;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.EquipmentSlot;
import com.snowygryphon.osrs.script_v2.streams.EquipmentStream;
import com.snowygryphon.osrs.script_v2.world.item.EquipmentItem;
import com.snowygryphon.osrs.script_v2.world.item.InventoryItem;

import java.util.Optional;
import java.util.stream.Collectors;

public class Equipment {
    private final Bot bot;
    private final Inventory inventory;

    public Equipment(Bot bot, Inventory inventory) {
        this.bot = bot;
        this.inventory = inventory;
    }


    public EquipmentStream items() {
        return new EquipmentStream(
                bot.getEquipment().getItems().values().stream().map(i -> new EquipmentItem(bot, i)).collect(Collectors.toList()), bot
        );
    }

    public Optional<EquipmentItem> slot(EquipmentSlot slot) {
        var item = bot.getEquipment().getItemInSlot(slot);
        if(item == null) return Optional.empty();
        return Optional.of(new EquipmentItem(bot, item));
    }

    public boolean slotEquipped(EquipmentSlot slot) {
        return this.slot(slot).isPresent();
    }

    public int countArrows() {
        return bot.getEquipment().getArrowCount();
    }

    public boolean wield(InventoryItem item) {
        if(item == null) throw new IllegalArgumentException("item is null");
        item.interact("Wield");
        return true;
    }

    public boolean wield(String item_name) {
        var item = inventory.random(item_name);
        if(!item.isPresent()) return false;
        return wield(item.get());
    }

    public boolean wield(int item_id) {
        var item = inventory.random(item_id);
        if(!item.isPresent()) return false;
        return wield(item.get());
    }

    public boolean remove(EquipmentSlot slot) {
        if(slot == null) throw new IllegalArgumentException("Failed to interact with slot");
        bot.getEquipment().remove(slot);
        return true;
    }
}
