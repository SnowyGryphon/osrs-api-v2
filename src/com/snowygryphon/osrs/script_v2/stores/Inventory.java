package com.snowygryphon.osrs.script_v2.stores;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.ItemAction;
import com.snowygryphon.osrs.script.WorldObject;
import com.snowygryphon.osrs.script_v2.streams.InventoryStream;
import com.snowygryphon.osrs.script_v2.world.item.InventoryItem;

import java.util.Optional;
import java.util.stream.Collectors;

public class Inventory {
    private final Bot bot;

    public Inventory(Bot bot) {
        this.bot = bot;
    }

    public InventoryStream items() {
        return new InventoryStream(
                bot.getInventory().getItems().stream().map(i -> new InventoryItem(bot, i)).collect(Collectors.toList()), bot
        );
    }

    public Optional<InventoryItem> random(String ... names) {
        return items().name(names).random();
    }

    public Optional<InventoryItem> random(int ... object_ids) {
        return items().id(object_ids).random();
    }

    public Optional<InventoryItem> slot(int slot_id) {
        return items().slot(slot_id);
    }

    public boolean contains(String ... names) {
        return items().name(names).first().isPresent();
    }

    public boolean contains(int ... object_ids) {
        return items().id(object_ids).first().isPresent();
    }

    public boolean slotOccupied(int slot_id) {
        return slot(slot_id).isPresent();
    }

    public int coinCount() {
        return bot.getInventory().getCoinCount();
    }

    public int count(String ... names) {
        return items().name(names).stdStream().mapToInt(InventoryItem::count).sum();
    }

    private boolean interact_impl(Optional<InventoryItem> item, String action) {
        if(item.isPresent()) {
            item.get().interact(action);
            return true;
        }
        return false;
    }

    public boolean interact(String item_name, String action) {
        return interact_impl(random(item_name), action);
    }

    public boolean interact(int object_id, String action) {
        return interact_impl(random(object_id), action);
    }

    public boolean interactSlot(int slot_id, String action) {
        return interact_impl(slot(slot_id), action);
    }

    public boolean interact(String item_name, int id, String str1, String str2) {
        var i = random(item_name);
        if(i.isPresent()) {
            i.get().interact(id, str1, str2);
            return true;
        }
        return false;
    }

    public boolean interact(String item_name, int id, String str1) {
        var i = random(item_name);
        if(i.isPresent()) {
            i.get().interact(id, str1);
            return true;
        }
        return false;
    }

    public boolean interact(String item_name, int id) {
        var i = random(item_name);
        if(i.isPresent()) {
            i.get().interact(id);
            return true;
        }
        return false;
    }

    public boolean interact(String item_name, ItemAction a) {
        var i = random(item_name);
        if(i.isPresent()) {
            i.get().interact(a);
            return true;
        }
        return false;
    }

    public boolean useOn(String item_name, InventoryItem other) {
        var i = random(item_name);
        if(i.isPresent()) {
            i.get().useOn(other);
            return true;
        }
        return false;
    }

    public boolean useOnInventory(String a_name, String subject_name) {
        var a = random(a_name);
        var b = random(subject_name);
        if(a.isPresent() && b.isPresent()) {
            a.get().useOn(b.get());
            return true;
        }
        return false;
    }

    public boolean useOn(String item_name, WorldObject obj) {
        var i = random(item_name);
        if(i.isPresent()) {
            i.get().useOn(obj);
            return true;
        }
        return false;
    }
}
