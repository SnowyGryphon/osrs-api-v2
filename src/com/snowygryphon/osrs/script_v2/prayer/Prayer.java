package com.snowygryphon.osrs.script_v2.prayer;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.PrayerData;

public class Prayer {
    private final Bot bot;
    private final PrayerData prayer;
    private final PrayerName name;

    public Prayer(Bot bot, PrayerData prayer, PrayerName name) {
        this.bot = bot;
        this.prayer = prayer;
        this.name = name;
    }

    public PrayerName name() { return name; }

    public boolean canActivate() { return prayer.canActivate(); }

    public boolean isActive() { return prayer.isActivated(); }

    public void activate() {
        bot.getPrayer().activate(prayer);
    }

    public void activateFast() {
        bot.getPrayer().deActivateFast(prayer);
    }

    public void deactivate() {
        bot.getPrayer().deActivate(prayer);
    }

    public void deactivateFast() {
        bot.getPrayer().deActivateFast(prayer);
    }
}
