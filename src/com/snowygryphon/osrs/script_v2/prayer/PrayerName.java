package com.snowygryphon.osrs.script_v2.prayer;

import java.util.HashMap;

public enum PrayerName {

    ThickSkin("Thick Skin"),
    BurstOfStrength("Burst of Strength"),
    ClarityOfThought("Clarity of Thought"),
    SharpEye("Sharp Eye"),
    MysticWill("Mystic Will"),
    RockSkin("Rock Skin"),
    SuperHumanStrength("Superhuman Strength"),
    ImprovedReflexes("Improved Reflexes"),
    RapidRestore("Rapid Restore"),
    RapidHeal("Rapid Heal"),
    ProtectItem("Protect Item"),
    HawkEye("Hawk Eye"),
    MysticLore("Mystic Lore"),
    SteelSkin("Steel Skin"),
    UltimateStrength("Ultimate Strength"),
    IncredibleReflexes("Incredible Reflexes"),
    ProtectFromMagic("Protect from Magic"),
    ProtectFromMissiles("Protect from Missiles"),
    ProtectFromMelee("Protect from Melee"),
    EagleEye("Eagle Eye"),
    MysticMight("Mystic Might"),
    Retribution("Retribution"),
            Redemption("Redemption"),
    Smite("Smite"),
            Preserve("Preserve"),
    Chivalry("Chivalry"),
            Piety("Piety"),
    Rigour("Rigour"),
            Augury("Augury"),
            ;
    public final String name;

    PrayerName(String name) {
        this.name = name;
    }

    public static final HashMap<String, PrayerName> NAME_ENUM_LOOKUP = create_lookup();

    private static HashMap<String, PrayerName> create_lookup() {
        var r = new HashMap<String, PrayerName>();
        for(var n : PrayerName.values()) {
            r.put(n.name, n);
        }
        return r;
    }
}
