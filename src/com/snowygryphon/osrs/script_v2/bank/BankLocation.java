package com.snowygryphon.osrs.script_v2.bank;

import java.util.HashMap;

public enum BankLocation {
    AL_KHARID(com.snowygryphon.osrs.script.BankLocation.AL_KHARID),
    ARCEUUS(com.snowygryphon.osrs.script.BankLocation.ARCEUUS),
    ARDOUGNE_NORTH(com.snowygryphon.osrs.script.BankLocation.ARDOUGNE_NORTH),
    ARDOUDNE_SOUTH(com.snowygryphon.osrs.script.BankLocation.ARGOUDNE_SOUTH),
    BARBARIAN_OUTPOST(com.snowygryphon.osrs.script.BankLocation.BARBARIAN_OUTPOST),
    BLAST_FURNACE_BANK(com.snowygryphon.osrs.script.BankLocation.BLAST_FURNACE_BANK),
    BLAST_MINE(com.snowygryphon.osrs.script.BankLocation.BLAST_MINE),
    CAMELOT(com.snowygryphon.osrs.script.BankLocation.CAMELOT),
    CANIFIS(com.snowygryphon.osrs.script.BankLocation.CANIFIS),
    CASTLE_WARS(com.snowygryphon.osrs.script.BankLocation.CASTLE_WARS),
    CATHERBY(com.snowygryphon.osrs.script.BankLocation.CATHERBY),
    DIHN_BANK(com.snowygryphon.osrs.script.BankLocation.DIHN_BANK),
    DRAYNOR(com.snowygryphon.osrs.script.BankLocation.DRAYNOR),
    DUEL_ARENA(com.snowygryphon.osrs.script.BankLocation.DUEL_ARENA),
    DWARF_MINE_BANK(com.snowygryphon.osrs.script.BankLocation.DWARF_MINE_BANK),
    EDGEVILLE(com.snowygryphon.osrs.script.BankLocation.EDGEVILLE),
    FALADOR_EAST(com.snowygryphon.osrs.script.BankLocation.FALADOR_EAST),
    FALADOR_WEST(com.snowygryphon.osrs.script.BankLocation.FALADOR_WEST),
    FARMING_GUILD(com.snowygryphon.osrs.script.BankLocation.FARMING_GUILD),
    FEROX_ENCLAVE(com.snowygryphon.osrs.script.BankLocation.FEROX_ENCLAVE),
    FISHING_GUILD(com.snowygryphon.osrs.script.BankLocation.FISHING_GUILD),
    FOSSIL_ISLAND(com.snowygryphon.osrs.script.BankLocation.FOSSIL_ISLAND),
    GNOME_BANK(com.snowygryphon.osrs.script.BankLocation.GNOME_BANK),
    GNOME_TREE_BANK_SOUTH(com.snowygryphon.osrs.script.BankLocation.GNOME_TREE_BANK_SOUTH),
    GNOME_TREE_BANK_WEST(com.snowygryphon.osrs.script.BankLocation.GNOME_TREE_BANK_WEST),
    GRAND_EXCHANGE(com.snowygryphon.osrs.script.BankLocation.GRAND_EXCHANGE),
    GREAT_KOUREND_CASTLE(com.snowygryphon.osrs.script.BankLocation.GREAT_KOUREND_CASTLE),
    HOSIDIUS(com.snowygryphon.osrs.script.BankLocation.HOSIDIUS),
    HOSIDIUS_KITCHEN(com.snowygryphon.osrs.script.BankLocation.HOSIDIUS_KITCHEN),
    JATIZSO(com.snowygryphon.osrs.script.BankLocation.JATIZSO),
    ISLE_OF_SOULS(com.snowygryphon.osrs.script.BankLocation.ISLE_OF_SOULS),
    LANDS_END(com.snowygryphon.osrs.script.BankLocation.LANDS_END),
    LOVAKENGJ(com.snowygryphon.osrs.script.BankLocation.LOVAKENGJ),
    LUMBRIDGE_BASEMENT(com.snowygryphon.osrs.script.BankLocation.LUMBRIDGE_BASEMENT),
    LUMBRIDGE_TOP(com.snowygryphon.osrs.script.BankLocation.LUMBRIDGE_TOP),
    LUNAR_ISLE(com.snowygryphon.osrs.script.BankLocation.LUNAR_ISLE),
    MOTHERLOAD(com.snowygryphon.osrs.script.BankLocation.MOTHERLOAD),
    MOUNT_KARUULM(com.snowygryphon.osrs.script.BankLocation.MOUNT_KARUULM),
    NARDAH(com.snowygryphon.osrs.script.BankLocation.NARDAH),
    NEITIZNOT(com.snowygryphon.osrs.script.BankLocation.NEITIZNOT),
    PEST_CONTROL(com.snowygryphon.osrs.script.BankLocation.PEST_CONTROL),
    PISCARILIUS(com.snowygryphon.osrs.script.BankLocation.PISCARILIUS),
    PRIFDDINAS(com.snowygryphon.osrs.script.BankLocation.PRIFDDINAS),
    ROGUES_DEN(com.snowygryphon.osrs.script.BankLocation.ROGUES_DEN),
    RUINS_OF_UNKAH(com.snowygryphon.osrs.script.BankLocation.RUINS_OF_UNKAH),
    SHANTY_PASS(com.snowygryphon.osrs.script.BankLocation.SHANTY_PASS),
    SHAYZIEN_CHEST(com.snowygryphon.osrs.script.BankLocation.SHAYZIEN_CHEST),
    SHAYZIEN_BANK(com.snowygryphon.osrs.script.BankLocation.SHAYZIEN_BANK),
    SHILO_VILLAGE(com.snowygryphon.osrs.script.BankLocation.SHILO_VILLAGE),
    SOPHANEM(com.snowygryphon.osrs.script.BankLocation.SOPHANEM),
    SULPHUR_MINE(com.snowygryphon.osrs.script.BankLocation.SULPHUR_MINE),
    TZHAAR(com.snowygryphon.osrs.script.BankLocation.TZHAAR),
    VARROCK_EAST(com.snowygryphon.osrs.script.BankLocation.VARROCK_EAST),
    VARROCK_WEST(com.snowygryphon.osrs.script.BankLocation.VARROCK_WEST),
    VINERY(com.snowygryphon.osrs.script.BankLocation.VINERY),
    VINERY_BANK(com.snowygryphon.osrs.script.BankLocation.VINERY_BANK),
    WINTERTODT(com.snowygryphon.osrs.script.BankLocation.WINTERTODT),
    WOODCUTTING_GUILD(com.snowygryphon.osrs.script.BankLocation.WOODCUTTING_GUILD),
    YANILLE(com.snowygryphon.osrs.script.BankLocation.YANILLE),
    ZANARIS(com.snowygryphon.osrs.script.BankLocation.ZANARIS),
    ZEAH_SAND_BANK(com.snowygryphon.osrs.script.BankLocation.ZEAH_SAND_BANK)
    ;

    public final Bank bank;
    public static final HashMap<com.snowygryphon.osrs.script.BankLocation, BankLocation> V1_V2_LOOKUP = create_lookup();

    private static HashMap<com.snowygryphon.osrs.script.BankLocation, BankLocation> create_lookup() {
        var r = new HashMap<com.snowygryphon.osrs.script.BankLocation, BankLocation>();
        for(var e : values()) {
            r.put(e.bank.backend, e);
        }
        return r;
    }

    BankLocation(com.snowygryphon.osrs.script.BankLocation l) {
        this.bank = new Bank(l);
    }
}
