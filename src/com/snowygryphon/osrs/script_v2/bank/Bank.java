package com.snowygryphon.osrs.script_v2.bank;

import com.snowygryphon.osrs.script_v2.props.Positionable;
import com.snowygryphon.osrs.script_v2.geometry.Position;

import java.util.Objects;

public class Bank extends Positionable {
    public final com.snowygryphon.osrs.script.BankLocation backend;

    public Bank(com.snowygryphon.osrs.script.BankLocation backend) {
        this.backend = backend;
    }

    public String name() {
        return backend.name();
    }

    @Override
    public Position tile() {
        return new Position(backend.getPosition());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank that = (Bank) o;
        return backend == that.backend;
    }

    @Override
    public int hashCode() {
        return Objects.hash(backend);
    }
}
