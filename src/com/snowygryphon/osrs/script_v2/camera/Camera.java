package com.snowygryphon.osrs.script_v2.camera;

import com.snowygryphon.osrs.script.Bot;

public class Camera {
    private final Bot bot;

    public Camera(Bot bot) {
        this.bot = bot;
    }

    public int xRot() {
        return bot.getCamera().getXRotation();
    }

    public int zRot() {
        return bot.getCamera().getZRotation();
    }
}
