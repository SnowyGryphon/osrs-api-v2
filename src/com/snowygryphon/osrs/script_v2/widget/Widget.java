package com.snowygryphon.osrs.script_v2.widget;

import com.snowygryphon.osrs.script.Bot;

import java.util.HashMap;
import java.util.List;

public class Widget {
    private final Bot bot;
    private final com.snowygryphon.osrs.script.Widget w;

    public Widget(Bot bot, com.snowygryphon.osrs.script.Widget w) {
        this.bot = bot;
        this.w = w;
    }

    public String text() {
        return w.getText();
    }

    public String text2() {
        return w.getText2();
    }

    public String mainAction() {
        return w.getMainAction();
    }

    public List<String> actions() {
        return w.getActions();
    }

    public boolean visible() {
        return w.isVisible();
    }

    public Widget parent() {
        var p = w.getParent();
        if(p == null) return null;
        return new Widget(bot, p);
    }

    public HashMap<Integer, Widget> children() {
        var r = new HashMap<Integer, Widget>();
        for(var e : w.getChildren().entrySet()) {
            r.put(e.getKey(), new Widget(bot, e.getValue()));
        }
        return r;
    }

    public List<Integer> idLine() {
        return w.getIDLine();
    }

    public Widget tryGetChildWidget(int... var1) {
        var r = w.tryGetChildWidget(var1);
        if(r == null) return null;
        return new Widget(bot, r);
    }

    public Widget tryGetChildWidget(Iterable<Integer> var1) {
        var r = w.tryGetChildWidget(var1);
        if(r == null) return null;
        return new Widget(bot, r);
    }

    public void interact() {
        bot.interactWith(w);
    }

    public void interact(String action) {
        if(action == null) throw new IllegalArgumentException("action is null");
        bot.interactWith(w, action);
    }
}
