package com.snowygryphon.osrs.script_v2.walk;

import com.snowygryphon.osrs.script.WebWalkPath;
import com.snowygryphon.osrs.script_v2.props.Positionable;

public class Walk {
    private final com.snowygryphon.osrs.script.Bot bot;

    public Walk(com.snowygryphon.osrs.script.Bot bot) {
        this.bot = bot;
    }

    public boolean direct(Positionable loc) {
        if(loc == null) throw new IllegalArgumentException("loc is null");
        return bot.walkTowardsTileOnMinimap(loc.tile().toVec2());
    }

    public boolean web(Positionable loc) {
        if(loc == null) throw new IllegalArgumentException("loc is null");
        return bot.webWalkTo(loc.tile().toWorldPos());
    }

    public boolean path(WebWalkPath path) {
        if(path == null) throw new IllegalArgumentException("path is null");
        return bot.getWebWalker().walkAlong(path);
    }
}
