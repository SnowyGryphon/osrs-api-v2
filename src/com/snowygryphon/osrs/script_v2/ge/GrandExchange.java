package com.snowygryphon.osrs.script_v2.ge;

import com.snowygryphon.osrs.script.*;
import com.snowygryphon.osrs.script.ge.GrandExchangeSearchItem;
import com.snowygryphon.osrs.script.ge.GrandExchangeSlot;
import com.snowygryphon.osrs.script.ge.SellCount;

import java.util.ArrayList;
import java.util.List;

public class GrandExchange {
    private final Bot bot;
    private final com.snowygryphon.osrs.script_v2.Bot v2;


    public GrandExchange(Bot bot, com.snowygryphon.osrs.script_v2.Bot v2) {
        this.bot = bot;
        this.v2 = v2;
    }

    private com.snowygryphon.osrs.script.ge.GrandExchange g() {
        return bot.getGrandExchange();
    }

    public com.snowygryphon.osrs.script.ge.GrandExchange getBackend() {
        return g();
    }

    public List<GrandExchangeSlot> getSlots() {
        return g().getSlots();
    }

    public String getSearchTextFieldText() {
        return g().getSearchTextFieldText();
    }

    public boolean openSearchWidget() {
        return g().openSearchWidget();
    }

    public boolean clearSearchWidget() {
        return g().clearSearchWidget();
    }

    public boolean setSearchWidgetText(String text) {
        if(text == null) throw new IllegalArgumentException("text is null");
        return g().setSearchWidgetText(text);
    }

    public boolean selectBuyItemWithName(String name) {
        if(name == null) throw new IllegalArgumentException("name is null");
        return g().selectBuyItemWithName(name);
    }

    public boolean collectToInventory() {
        return g().collectToInventory();
    }

    public boolean collectToBank() {
        return g().collectToBank();
    }

    public boolean isPriceInputOpen() {
        return isPriceInputOpen();
    }

    public boolean isQuantityInputOpen() {
        return isQuantityInputOpen();
    }

    public boolean openPriceInput() {
        return g().openPriceInput();
    }

    public boolean openQuantityInput() {
        return g().openQuantityInput();
    }

    public boolean setPrice(int prize) {
        return g().setPrice(prize);
    }

    public boolean setQuantity(int q) {
        return g().setQuantity(q);
    }

    public boolean clickConfirmButton() {
        return g().clickConfirmButton();
    }

    public boolean buyItem(String item, int prize, int quantity) {
        if(item == null) throw new IllegalArgumentException("item is null");
        return g().buyItem(item, prize, quantity);
    }

    public boolean buyItem(int slot_idx, String item, int prize, int quantity) {
        return g().buyItem(slot_idx, item, prize, quantity);
    }

    public boolean buyItem(GrandExchangeSlot slot, String item, int prize, int quantity) {
        return this.buyItem(slot.getSlotId(), item, prize, quantity);
    }

    public boolean offerInventoryItem(InventoryItem item) {
        if(item == null) throw new IllegalArgumentException("item is null");
        return g().offerInventoryItem(item);
    }

    public boolean offerInventoryItem(com.snowygryphon.osrs.script_v2.world.item.InventoryItem item) {
        if(item == null) throw new IllegalArgumentException("item is null");
        return offerInventoryItem(item.getBackend());
    }

    public boolean offerInventoryItem(String item) {
        if(item == null) throw new IllegalArgumentException("item is null");
        var i = v2.inventory().random(item);
        return i.filter(this::offerInventoryItem).isPresent();
    }

    public boolean clickAllButton() {
        return g().clickAllButton();
    }

    public boolean sellItem(InventoryItem item, int prize, SellCount count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        if(count == null) throw new IllegalArgumentException("count is null");
        return g().sellItem(item, prize, count);
    }

    public boolean sellItem(com.snowygryphon.osrs.script_v2.world.item.InventoryItem item, int prize, SellCount count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        return sellItem(item.getBackend(), prize, count);
    }

    public boolean sellItem(String item, int prize, SellCount count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        var i = v2.inventory().random(item);
        return i.filter(a -> sellItem(a, prize, count)).isPresent();
    }

    public boolean sellItem(int slot_idx, InventoryItem item, int prize, SellCount count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        if(count == null) throw new IllegalArgumentException("count is null");
        return g().sellItem(slot_idx, item, prize, count);
    }

    public boolean sellItem(int slot_idx, com.snowygryphon.osrs.script_v2.world.item.InventoryItem item, int prize, SellCount count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        return sellItem(slot_idx, item.getBackend(), prize, count);
    }

    public boolean sellItem(int slot_idx, String item, int prize, SellCount count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        var i = v2.inventory().random(item);
        return i.filter(a -> sellItem(slot_idx, a, prize, count)).isPresent();
    }

    public boolean sellItem(GrandExchangeSlot slot, InventoryItem item, int prize, SellCount count) {
        return this.sellItem(slot.getSlotId(), item, prize, count);
    }

    public boolean sellItem(GrandExchangeSlot slot, com.snowygryphon.osrs.script_v2.world.item.InventoryItem item, int prize, SellCount count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        if(slot == null) throw new IllegalArgumentException("slot is null");
        return sellItem(slot, item.getBackend(), prize, count);
    }

    public boolean sellItem(GrandExchangeSlot slot, String item, int prize, SellCount count) {
        if(item == null) throw new IllegalArgumentException("item is null");
        if(slot == null) throw new IllegalArgumentException("slot is null");
        var i = v2.inventory().random(item);
        return i.filter(a -> sellItem(slot, a, prize, count)).isPresent();
    }

    public String getSelectedItemName() {
        return g().getSelectedItemName();
    }

    public int getPricePerItem() {
        return g().getPricePerItem();
    }

    public int getQuantity() {
        return g().getQuantity();
    }

    public ArrayList<GrandExchangeSearchItem> getItemSearchItems() {
        return g().getItemSearchItems();
    }

    public boolean isOpen() {
        return g().isOpen();
    }

    public com.snowygryphon.osrs.script.ge.GrandExchange.State getState() {
        return g().getState();
    }

    public boolean interactWithClerk() {
        return g().interactWithClerk();
    }
}
