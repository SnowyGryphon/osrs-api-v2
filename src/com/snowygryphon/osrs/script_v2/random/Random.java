package com.snowygryphon.osrs.script_v2.random;

import com.snowygryphon.osrs.script.Bot;

public class Random {
    private final Bot bot;
    private Algorithm algorithm = Algorithm.Gausian;

    public enum Algorithm {
        Uniform,
        Gausian
    }

    public Random(Bot bot) {
        this.bot = bot;
    }

    public Algorithm preferredAlgo() {
        return algorithm;
    }

    public void preferredAlgo(Algorithm a) {
        if(a == null) throw new IllegalArgumentException("a is null");
        algorithm = a;
    }

    /**
     * Returns a random number using the preferred algorithm, which by default is gausian.
     * @param min
     * @param max
     * @return
     */
    public int next(int min, int max) {
        switch (algorithm) {
            case Gausian:
                return gausian(min, max);
            case Uniform:
                return uniform(min, max);
            default:
                throw new IllegalStateException("Unhandled enum value " + algorithm.name());
        }
    }

    public int uniform(int min, int max) {
        if(min > max) {
            min = max;
        } else if(max < min) {
            max = min;
        }

        if(min == max) return min;
        return bot.getRandom().nextInt(max - min) + min;
    }

    public int gausian(int min, int max) {
        if(min > max) {
            min = max;
        } else if(max < min) {
            max = min;
        }

        if(min == max) return min;
        var delta = max - min;

        return (int)( bot.getRandom().nextGaussian()*delta+min);
    }
}
