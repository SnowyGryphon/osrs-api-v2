package com.snowygryphon.osrs.script_v2.log;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.LogSeverity;

public class Log {
    private final Bot bot;

    public Log(Bot bot) {
        this.bot = bot;
    }

    public void log(LogSeverity s, Object msg) {
        if(s == null) throw new IllegalArgumentException("s is null");
        if(msg == null) throw new IllegalArgumentException("msg is null");
        bot.log(s, msg.toString());
    }

    public void msg(Object msg) {
        if(msg == null) throw new IllegalArgumentException("msg is null");
        bot.logInfo(msg.toString());
    }

    public void info(Object msg) {
        msg(msg);
    }

    public void success(Object msg) {
        if(msg == null) throw new IllegalArgumentException("msg is null");
        bot.logSuccess(msg.toString());
    }

    public void warning(Object msg) {
        if(msg == null) throw new IllegalArgumentException("msg is null");
        bot.logWarning(msg.toString());
    }

    public void logError(Object msg) {
        if(msg == null) throw new IllegalArgumentException("msg is null");
        bot.logError(msg.toString());
    }
}
