package com.snowygryphon.osrs.script_v2.path;

import com.snowygryphon.osrs.script.*;
import com.snowygryphon.osrs.script_v2.bank.Bank;
import com.snowygryphon.osrs.script_v2.props.Positionable;

public class Pather {
    private final Bot bot;

    public Pather(Bot bot) {
        this.bot = bot;
    }

    public WebWalkPath queryPath(Positionable var1, Positionable var2) {
        return bot.getWebWalker()
                .createPathInstance(
                        bot.getPathCalculator().queryPath(
                                var1.tile().toWorldPos(), var2.tile().toWorldPos()));
    }

    public WebWalkPath queryBank(Positionable var1, Bank var2) {
        return bot.getWebWalker()
                .createPathInstance(
                        bot.getPathCalculator().queryBank(
                                var1.tile().toWorldPos(), var2.backend));
    }

    public WebWalkPath queryBank(Positionable var1, BankLocation bank) {
        return queryBank(var1, new Bank(bank));
    }

    public WebWalkPath queryBank(Positionable var1, com.snowygryphon.osrs.script_v2.bank.BankLocation bank) {
        return queryBank(var1, bank.bank);
    }

}
