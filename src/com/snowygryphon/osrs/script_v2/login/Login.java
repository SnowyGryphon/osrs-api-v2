package com.snowygryphon.osrs.script_v2.login;

import com.snowygryphon.osrs.script.Bot;
import com.snowygryphon.osrs.script.ConnectionState;
import com.snowygryphon.osrs.script.WelcomeWindow;

public class Login {
    private final Bot bot;

    public Login(Bot bot) {
        this.bot = bot;
    }

    public void clickLogin() {
        bot.clickLoginButton();
    }

    public void clickOK() {
        bot.clickDisconnectedOKButton();
    }

    public ConnectionState connectionState() {
        return bot.getConnectionState();
    }

    public WelcomeWindow welcomeWindow() {
        return bot.getStdWindows().getWelcomeWindow();
    }
}
