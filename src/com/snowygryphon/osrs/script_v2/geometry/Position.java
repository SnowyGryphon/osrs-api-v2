package com.snowygryphon.osrs.script_v2.geometry;

import com.snowygryphon.osrs.script.WorldPos;
import com.snowygryphon.osrs.script_v2.props.Positionable;
import org.joml.Vector2f;
import org.joml.Vector2i;

import java.util.Objects;

public class Position extends Positionable {
    public int x, y, z;

    public Position() {}
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public Position(Vector2i v) {
        if(v == null) throw new IllegalArgumentException("v is null");
        this.x = v.x;
        this.y = v.y;
    }
    public Position(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Position(Vector2i v, int z) {
        if(v == null) throw new IllegalArgumentException("v is null");
        this.x = v.x;
        this.y = v.y;
        this.z = z;
    }
    public Position(Positionable other) {
        if(other == null) throw new IllegalArgumentException("other is null");
        var t = other.tile();
        this.x = t.x;
        this.y = t.y;
        this.z = t.z;
    }
    public Position(WorldPos other) {
        if(other == null) throw new IllegalArgumentException("other is null");
        this.x = other.getTile().x;
        this.y = other.getTile().y;
        this.z = other.getZ();
    }

    public Position(com.snowygryphon.osrs.script.Tile snowy_tile, int z) {
        this.x = snowy_tile.x;
        this.y = snowy_tile.y;
        this.z = z;
    }

    @Override
    public Position tile() {
        return this;
    }

    public void set(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Position set(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public Position set(Positionable o) {
        if(o == null) throw new IllegalArgumentException("o is null");
        return set(o.tile().x, o.tile().y);
    }

    public Position add(int x, int y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public Position add(Position t) {
        if(t == null) throw new IllegalArgumentException("t is null");
        return add(t.x, t.y);
    }

    public Position sub(int x, int y) {
        return add(-x, -y);
    }

    public Position sub(Position t) {
        if(t == null) throw new IllegalArgumentException("t is null");
        return sub(t.x, t.y);
    }

    public Position mul(int x, int y) {
        this.x *= x;
        this.y *= y;
        return this;
    }

    public Position mul(Position t) {
        if(t == null) throw new IllegalArgumentException("t is null");
        return mul(t.x, t.y);
    }

    public Position moveTowards(Positionable p, float max_distance) {
        Position start_point = this;
        Position end_point = p.tile();
        if(start_point.distanceTo(end_point) >= max_distance) {
            Vector2f new_pos = directionVector(p).mul(max_distance);

            return add((int)new_pos.x, (int)new_pos.y);
        } else {
            return set(end_point);
        }
    }

    public Vector2f directionVector(Positionable p) {
        return toVec2F()
                .sub(p.tile().toVec2F())
                .normalize();
    }

    public Position moveInDirection(Positionable p, float distance) {
        var offset = directionVector(p).mul(distance);
        return add((int)offset.x, (int)offset.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position tile = (Position) o;
        return x == tile.x &&
                y == tile.y &&
                z == tile.z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    public Vector2i toVec2() {
        return new Vector2i(x, y);
    }

    public Vector2f toVec2F() {
        return new Vector2f(x, y);
    }

    public WorldPos toWorldPos() {
        return new WorldPos(x, y, z);
    }
}
