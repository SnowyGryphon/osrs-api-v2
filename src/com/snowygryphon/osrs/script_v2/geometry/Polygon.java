package com.snowygryphon.osrs.script_v2.geometry;

import java.util.Arrays;

public class Polygon {
    public static boolean contains(Position test, Position[] points) {
        if(test == null) throw new IllegalArgumentException("test is null");
        if(points == null) throw new IllegalArgumentException("points is null");
        if(points.length == 0) throw new IllegalArgumentException("points is empty");
        int i;
        int j;
        boolean result = false;
        for (i = 0, j = points.length - 1; i < points.length; j = i++) {
            if ((points[i].y > test.y) != (points[j].y > test.y) &&
                    (test.x < (points[j].x - points[i].x) * (test.y - points[i].y) / (points[j].y-points[i].y) + points[i].x)) {
                result = !result;
            }
        }
        return result;
    }

    public static Box box(Position[] points) {
        if(points == null) throw new IllegalArgumentException("points is null");
        if(points.length == 0) throw new IllegalArgumentException("points is empty");
        var x_min = Arrays.stream(points).map(p -> p.x).min(Integer::compareTo).orElse(0);
        var x_max = Arrays.stream(points).map(p -> p.x).max(Integer::compareTo).orElse(0);
        var y_min = Arrays.stream(points).map(p -> p.y).min(Integer::compareTo).orElse(0);
        var y_max = Arrays.stream(points).map(p -> p.y).max(Integer::compareTo).orElse(0);
        return new Box(new Position(x_min, y_min), new Position(x_max, y_max));
    }
}
