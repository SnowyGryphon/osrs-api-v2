package com.snowygryphon.osrs.script_v2.geometry;

import com.snowygryphon.osrs.script_v2.props.Positionable;

public class Box {
    public final Position bottom_left, top_right;

    public Box(Position bottom_left, Position top_right) {
        if(bottom_left == null) throw new IllegalArgumentException("bottom_left is null");
        if(top_right == null) throw new IllegalArgumentException("top_right is null");
        this.bottom_left = bottom_left;
        this.top_right = top_right;
    }

    public boolean contains(Positionable p) {
        var t = p.tile();
        if(t.x >= bottom_left.x && t.y >= bottom_left.y) {
            return t.x <= top_right.x && t.y <= top_right.y;
        }
        return false;
    }

    public Position center() {
        var r = new Position((top_right.x - bottom_left.x) / 2, (top_right.y - top_right.x) / 2); // Offset middle
        r.add(bottom_left);
        return r;
    }
}
