package com.snowygryphon.osrs.script_v2.geometry;

import com.snowygryphon.osrs.script.Tile;
import com.snowygryphon.osrs.script.WorldObject;
import com.snowygryphon.osrs.script.WorldPos;
import com.snowygryphon.osrs.script_v2.Bot;
import com.snowygryphon.osrs.script_v2.props.Positionable;
import org.joml.Vector2i;

import java.util.HashSet;

public class Area {
    private final HashSet<Position> tiles = new HashSet<>();

    public Area() {}

    public Area(Area o) {
        if(o == null) throw new IllegalArgumentException("o is null");
        addArea(o);
    }

    public Area(int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y) {
        addBoxTopLeft(top_left_x, top_left_y, bottom_right_x, bottom_right_y);
    }

    public Area(Position top_left, Position bottom_right) {
        addBox(top_left, bottom_right);
    }

    public Area(Position[] positions) {
        addPolygon(positions);
    }

    public Area(int[][] points) {
        addPolygon(points);
    }

    public Area(Area[] areas) {
        if(areas == null) throw new IllegalArgumentException("areas is null");
        for(var a : areas) {
            if(a == null) throw new IllegalArgumentException("area element is null");
            addArea(a);
        }
    }

    public Area addTile(Positionable p) {
        if(p == null) throw new IllegalArgumentException("p is null");
        tiles.add(p.tile());
        return this;
    }

    public Area addBox(int min_x, int max_x, int min_y, int max_y) {
        for(int x = min_x; x <= max_x; ++x) {
            for(int y = min_y; y <= max_y; ++y) {
                tiles.add(new Position(x, y));
            }
        }
        return this;
    }

    public Area addBox(Positionable bottom_left, Positionable top_right) {
        if(bottom_left == null) throw new IllegalArgumentException("bottom_left is null");
        if(top_right == null) throw new IllegalArgumentException("top_right is null");
        if(!bottom_left.samePlane(top_right)) throw new IllegalArgumentException("bottom_left and top_right is on different planes");
        if(!bottom_left.tile().samePlane(top_right)) throw new IllegalArgumentException("Positions are not on same plane!");
        return addBox(bottom_left.tile().x, top_right.tile().x, bottom_left.tile().y, top_right.tile().y);
    }

    public Area addBoxTopLeft(Position top_left, Position bottom_right) {
        if(top_left == null) throw new IllegalArgumentException("bottom_left is null");
        if(bottom_right == null) throw new IllegalArgumentException("top_right is null");
        if(!top_left.samePlane(bottom_right)) throw new IllegalArgumentException("top_left and bottom_right is on different planes");

        var bottom_left = new Position(top_left.x, bottom_right.y);
        var top_right = new Position(bottom_right.x, top_left.y);
        return addBox(bottom_left, top_right);
    }

    public Area addBoxTopLeft(int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y) {
        return addBoxTopLeft(new Position(top_left_x, top_left_y), new Position(bottom_right_x, bottom_right_y));
    }

    public Area addCircle(Positionable center_p, double radius) {
        if(center_p == null) throw new IllegalArgumentException("center_p is null");

        Position c = center_p.tile();
        int radius_int = (int)(radius + 1);
        int min_x = c.x - radius_int;
        int max_x = c.x + radius_int;
        int min_y = c.y - radius_int;
        int max_y = c.y + radius_int;

        var tmp = new Position();

        for(int x = min_x; x <= max_x; ++x) {
            for(int y = min_y; y <= max_y; ++y) {
                tmp.set(x, y);
                if(tmp.distanceTo(c) <= radius_int) {
                    tiles.add(new Position(x, y));
                }
            }
        }
        return this;
    }

    public Area addPolygon(Position[] polygon) {
        if(polygon == null) throw new IllegalArgumentException("polygon is null");
        if(polygon.length == 0) throw new IllegalArgumentException("polygon is empty");

        var b = Polygon.box(polygon);
        var min_x = b.bottom_left.x;
        var min_y = b.bottom_left.y;
        var max_x = b.top_right.x;
        var max_y = b.top_right.y;

        var tmp = new Position();
        for(int x = min_x; x <= max_x; ++x) {
            for(int y = min_y; y <= max_y; ++y) {
                tmp.set(x, y);
                if(Polygon.contains(tmp, polygon)) {
                    tiles.add(new Position(x, y));
                }
            }
        }

        return this;
    }

    public Area addPolygon(int[][] points) {
        if(points == null) throw new IllegalArgumentException("points is null");
        if(points.length == 0) throw new IllegalArgumentException("points is empty");
        var tmp = new Position[points.length];
        for(int i = 0; i < points.length; ++i) {
            var p = points[i];
            if(p == null) throw new IllegalArgumentException("entry in points is null");
            if(p.length != 2) throw new IllegalArgumentException("entry in points is not 2 elements");
            tmp[i] = new Position(p[0], p[1]);
        }
        return addPolygon(tmp);
    }

    public Area addArea(Area a) {
        if(a == null) throw new IllegalArgumentException("a is null");
        tiles.addAll(a.tiles);
        return this;
    }

    public Area setPlane(int z) {
        for(var t : tiles) {
            t.z = z;
        }
        return this;
    }

    public boolean contains(Positionable p) {
        if(p == null) throw new IllegalArgumentException("p is null");
        return tiles.contains(p.tile());
    }

    public boolean contains(WorldObject obj) {
        if(obj == null) throw new IllegalArgumentException("obj is null");
        return contains(new Position(obj.getTile()));
    }

    public boolean contains(Vector2i tile) {
        if(tile == null) throw new IllegalArgumentException("tile is null");
        return contains(new Position(tile));
    }

    public boolean contains(WorldPos pos) {
        if(pos == null) throw new IllegalArgumentException("pos is null");
        return contains(new Position(pos));
    }

    public Position closestTo(Positionable p) {
        double closest_distance = Double.MAX_VALUE;
        Position current_closest = null;

        for(Position t : tiles) {
            var d = t.distanceTo(p);
            if(d < closest_distance) {
                current_closest = t;
                closest_distance = d;
            }
        }

        return current_closest;
    }

    public Position closestTo(WorldObject obj) {
        if(obj == null) throw new IllegalArgumentException("obj is null");
        return closestTo(new Position(obj.getTile()));
    }

    public Position closestTo(Vector2i tile) {
        if(tile == null) throw new IllegalArgumentException("tile is null");
        return closestTo(new Position(tile));
    }

    public Position closestTo(WorldPos pos) {
        if(pos == null) throw new IllegalArgumentException("pos is null");
        return closestTo(new Position(pos));
    }

    public Position closest() {
        var lp = Bot.threadlocal().localPlayer();
        if(lp.isEmpty()) return new Position();
        return closestTo(lp.get());
    }

    public Position bottomLeft() {
        int x = Integer.MAX_VALUE;
        int y = Integer.MAX_VALUE;
        for(var t : tiles) {
            if(t.x < x) x = t.x;
            if(t.y < y) y = t.y;
        }
        return new Position(x, y);
    }

    public Position topRight() {
        int x = -Integer.MAX_VALUE;
        int y = -Integer.MAX_VALUE;
        for(var t : tiles) {
            if(t.x > x) x = t.x;
            if(t.y > y) y = t.y;
        }
        return new Position(x, y);
    }

    public Position center() {
        return new Box(bottomLeft(), topRight()).center();
    }

    public Position mostCentral() {
        var center = center();
        Position ct = null;
        double cd = Double.MAX_VALUE;
        for(var t : tiles) {
            var d = center.distanceTo(t);
            if(d < cd) {
                ct = t;
                cd = d;
            }
        }
        if(ct != null) return new Position(ct);
        return null;
    }

    public Position random() {
        if(tiles.size() == 0) return null;
        if(tiles.size() == 1) return tiles.stream().findFirst().get();
        return tiles.stream()
                .skip(Bot.threadlocal().random().uniform(0, tiles.size()))
                .findFirst().get();
    }
}
